/*
   Code written by Zachary Whitlock

   Uses direct manipulation to setup interrupts for encoder inputs to
   SAMD21 ARM MCU.

   08/2020
*/

uint8_t stat = 0x00;

void setup() {
  SerialUSB.begin(115200);
  // CLOCK SETUP
  // Enable and configure generic clock generator 5
  // Note that generators 0-3 are used by the IDE
  GCLK->GENCTRL.reg = GCLK_GENCTRL_IDC |            // Improve duty cycle
                      GCLK_GENCTRL_GENEN |          // Enable generic clock gen
                      GCLK_GENCTRL_SRC_DFLL48M |    // Select 48MHz as source
                      GCLK_GENCTRL_ID(5);           // Select GCLK5
  while (GCLK->STATUS.bit.SYNCBUSY);      // Wait for synchronization

  // Set clock divider of 1 to generic clock generator 5
  GCLK->GENDIV.reg = GCLK_GENDIV_DIV(1) |           // Divide 48 MHz by 1
                     GCLK_GENDIV_ID(5);             // Apply to GCLK4 5
  while (GCLK->STATUS.bit.SYNCBUSY);      // Wait for synchronization


  // Enable GCLK4 and connect it to EIC
  GCLK->CLKCTRL.reg = GCLK_CLKCTRL_CLKEN |    // Enable generic clock
                      GCLK_CLKCTRL_GEN_GCLK5 |       // Select GCLK5
                      0x0005;                      // Feed GCLK5 to EIC
  while (GCLK->STATUS.bit.SYNCBUSY);          // Wait for synchronization

  EIC->CTRL.bit.SWRST = 1;
  while (EIC->STATUS.bit.SYNCBUSY);          // Wait for synchronization

  // Set input pins
  PORT->Group[0].DIR.bit.DIR |= 1 << 7;     // I too, like to name child
  PORT->Group[1].DIR.bit.DIR |= 1 << 9;     // variables the same as the
  //                                        // parent.

  // Setup pins with config register in PORT
  PORT->Group[0].WRCONFIG.reg = 0x50070080;
  PORT->Group[1].WRCONFIG.reg = 0x50070200;
  while (EIC->STATUS.bit.SYNCBUSY);          // Wait for synchronization

  // Setup interrupt pins in EIC
  EIC->INTENSET.bit.EXTINT7 = 1;
  EIC->INTENSET.bit.EXTINT9 = 1;
  while (EIC->STATUS.bit.SYNCBUSY);          // Wait for synchronization

  // Setup EIC config
  EIC->CONFIG[0].bit.FILTEN7 = 0;
  EIC->CONFIG[0].bit.SENSE7 = 0b001;
  EIC->CONFIG[1].bit.FILTEN1 = 0;
  EIC->CONFIG[1].bit.SENSE1 = 0b001;
  while (EIC->STATUS.bit.SYNCBUSY);          // Wait for synchronization

  // Enable CLK_EIC                       - X
  // PINCFG4, PMUXEN = 1                  - X
  // PMUX3.PMUXE = 0x00                   - X
  // EIC.CTRL.ENABLE = 1                  - X
  // CONFIG0.FILTEN = 0                   - X
  // CONFIG0.SENSE4 = 0b001 for RISING    - X
  // INTENSET.EXTINTx to 1                - X

  EIC->CTRL.bit.ENABLE = 1;
  while (EIC->STATUS.bit.SYNCBUSY);          // Wait for synchronization

  // Set pins to PULL-UP
  PORT->Group[0].OUT.reg |= 1 << 7;     // I too, like to name child
  PORT->Group[1].OUT.reg |= 1 << 9;     // variables the same as the


  // Configure interrupt request
  // See https://static.docs.arm.com/ddi0419/d/DDI0419D_armv6m_arm.pdf
  NVIC_DisableIRQ(EIC_IRQn);
  NVIC_ClearPendingIRQ(EIC_IRQn);
  NVIC_SetPriority(EIC_IRQn, 0);
  NVIC_EnableIRQ(EIC_IRQn);
}

void loop() {
  // put your main code here, to run repeatedly:
  if (stat > 0)
  {
    SerialUSB.println("Got interrupt.");
    stat = 0;
  }

  delay(1000);
  SerialUSB.print("EXTINT9: ");
  SerialUSB.println(EIC->INTFLAG.bit.EXTINT9);
  SerialUSB.print("EXTINT7: ");
  SerialUSB.println(EIC->INTFLAG.bit.EXTINT7);
}


// Handles interrupts for the EIC system
void EIC_Handler(void)
{
  // inside of interrupt, set INTFLAG.EXTINTx to 0
  EIC->INTFLAG.bit.EXTINT9 = 0;
  EIC->INTFLAG.bit.EXTINT7 = 0;

  // toggle status
  stat = 10;
}
