/*
 * eventFIFO.cpp by Zachary Whitlock
 * 
 * Written for the samd21 VFO, a ham radio project.
 * 
 */

#include "eventFIFO.h"

// Function to read from FIFO
// IF current-read < current-write
// 	Read from location pointed to by current-read
// 	IF not @ size
// 		Increment current-read location by 1
// 	ELSE
// 		Set current-read location to 0
// Reset event_wrap flag

// Fuction to write to FIFO
// Write to location @ current-write*
// IF not @ size
// 	Increment current-write location by 1
// ELSE
// 	Set current-write location to 0
// 	Set the loop-around flag
// 
// * If both the new event and the previous event were
// 'press' events, and the time-since-last-event is appropriate,
// delete both by reading them and write a 'double' event. 
//
// Update time since last event
//

EventFIFO FIFO;

extern volatile bool buttonDown;

event EventRead() {
	event readResult;
	if (FIFO.last_read != FIFO.last_write) {
		readResult = FIFO.buf[FIFO.last_read];
		FIFO.last_read++;
		if (FIFO.last_read > T_SIZE-1)
			FIFO.last_read = 0;
	} else {
		return NO_EVENT;
	}

	FIFO.event_wrap = false;
	return readResult;
}

void EventWrite(event inputEvent) {
	
	// Check if we have a button HELD state,
	// if we DO and we get a TURN event, send a HELD_TURN event
	// TODO!
	if (buttonDown && (inputEvent == TURN_LEFT || inputEvent == TURN_RIGHT)) {
		FIFO.buf[FIFO.last_write] = HELD_TURN;
	} else {
		FIFO.buf[FIFO.last_write] = inputEvent;
	}
	
	FIFO.last_write++;
	if (FIFO.last_write > T_SIZE-1) {
		FIFO.last_write = 0;
		FIFO.event_wrap = true;
	}
}

