/*
 * eventFIFO.h by Zachary Whitlock
 * 
 * Written for the samd21 VFO, a ham radio project.
 * 
 */

#include "Arduino.h"

// The FIFO buffer is not supposed to loop around to the beginning. It
// is supported, however, but will raise a flag.

// Event Enum
// * turn-left
// * turn-right
// * press
// * held
// * double

// Define a struct for the core of the FIFO
//  * Event buffer
//  * Time since last event
//  * Index of current read
//  * Index of current write
//  * Boolean write-loop-around


// Function to read from FIFO
// IF current-read < current-write
// 	Read from location pointed to by current-read
// 	IF not @ size
// 		Increment current-read location by 1
// 	ELSE
// 		Set current-read location to 0
// Reset event_wrap flag

// Fuction to write to FIFO
// Write to location @ current-write*
// IF not @ size
// 	Increment current-write location by 1
// ELSE
// 	Set current-write location to 0
// 	Set the loop-around flag
// 
// * If both the new event and the previous event were
// 'press' events, and the time-since-last-event is appropriate,
// delete both by reading them and write a 'double' event. 
//
// Update time since last event

#ifndef EVENTFIFO_H_INCLUDED
#define EVENTFIFO_H_INCLUDED
const int T_SIZE  = 10; // Total size - number of events


enum event {
	TURN_LEFT,
	TURN_RIGHT,
	PRESS,
	HELD,
	HELD_TURN,
	DOUBLE,
	NO_EVENT
};

struct EventFIFO {
	event buf[T_SIZE];
	int last_read; 	// index of last_read
	int last_write;	// index of last_write
	unsigned int time_since; // time since last event
	bool event_wrap; // Wrap-around flag
};


void EventWrite(event inputEvent);
event EventRead();
#endif
