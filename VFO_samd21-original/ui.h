/*
 * ui.h by Zachary Whitlock
 *
 * Written for the samd21 VFO, a ham radio project.
 *
 */


// Things we need to store in EEPROM:
// Clock values
// 	0 - 4B
// 	1 - 4B
//	2 - 4B
// Setting values for:
// 	Power		- 1B
// 	Phase		- 4B
//	Phase Enable	- 1B
// 	Step Value	- 1B
//	Entry Mode	- 1B
// 	Clock Enable 0	- 1B
// 	Clock Enable 1	- 1B
// 	Clock Enable 2	- 1B
// Total bytes: 23
// If everything is padded to 4 bytes (1 register)
// Total: 44B
// It's probably best to pad everything to 4 bytes.
// That leaves 20 bytes for other stuff in 1 page
// PLENTY of room, store strings?
// Strings also aren't THAT big.

// Displaying settings:
//

// Displaying Clock
//
#include "Arduino.h"
#include "eventFIFO.h"
#include "display.h"
#include "eepromSim.h"

#define ENC_BTN_PIN 7
#define ENC_A_PIN   8
#define ENC_B_PIN   9

#define UI_FREQ_X   20
#define UI_FREQ_Y   15

// Placing the clock indicators
#define UI_CLK_PLACE_X  45
#define UI_CLK_PLACE_Y  35

#define UI_CLK_SIZE_X  10
#define UI_CLK_SIZE_Y  8

#define UI_CLK_DELTA_X  13
#define UI_CLK_DELTA_Y  5

#define UI_BTN_DB   100
//#define UI_

#define UI_VFO_CHARS  10
#define UI_CLOCKS     3

// UI Settings page options
#define SETTING_STR_LEN 20
#define SETTING_VAL_LEN 5	// Changing can break struct padding.
#define SETTING_NUM 11
#define SETTING_STEP_MAX 40000
#define SETTING_V_SPACING 15
#define SETTING_X_MARGIN 2
#define SETTING_V_LINE_X 75

// EEPROM Related
#define UI_EEPROM_ROW 1015

// UI VFO Page options
#define VFO_ACTIVE_Y_POS 14	// Y position of big text
#define VFO_Y_DIST 21 		// Distance from big text to lower little text
#define VFO_X_MARGIN 4

enum displayState {
	SETTINGS_PAGE,
	VFO_PAGE
};

// Settings enums
enum settingEnum {
	S_POWER,
	S_PHASE,
	S_PHASE_EN,
	S_STEP,
	S_ENTRY_M,
	S_CLK0_EN,
	S_CLK1_EN,
	S_CLK2_EN,
	S_CLK0_INV,
	S_CLK1_INV,
	S_CLK2_INV

};

enum enumEntryMode {
	MODE_STEP,
	MODE_EDIT
};

enum powerState {
	POWER_8MA,
	POWER_6MA,
	POWER_4MA,
	POWER_2MA
};

// This is done C-style, to be later rewritten in C++ or rust style.
// AKA, I'm an idiot and didn't do this with Object-orientated programming.
// Anyway, I've padded all of the structs to be the same size so I can
// use them interchangably. Fun fact, the ordering of the sub-components
// of the structures changes their sizes!
typedef struct {
	char name[SETTING_STR_LEN];		// 1 byte * SETTING_STR_LEN
	char valueStr[SETTING_VAL_LEN];		// 1 byte * SETTING_VAL_LEN
	int filler;				// 4 bytes
	settingEnum settingType;		// 1 byte
	powerState value;			// 1 byte, padded to 4
} UI_powerSetting;				// 8 total + standard

typedef struct {
	char name[SETTING_STR_LEN];		// 1 byte * SETTING_STR_LEN
	char valueStr[SETTING_VAL_LEN]; 	// 1 byte * SETTING_VAL_LEN
	int value;				// 4 bytes
	settingEnum settingType;		// 1 byte, padded to 4
} UI_intSetting;				// 8 total + standard

typedef struct {
	char name[SETTING_STR_LEN];		// 1 byte * SETTING_STR_LEN
	char valueStr[SETTING_VAL_LEN];		// 1 byte * SETTING_VAL_LEN
	int filler;				// 4 bytes
	settingEnum settingType;		// 1 byte
	bool value;				// 1 byte
} UI_boolSetting;				// 8 total + standard

typedef struct {
	char name[SETTING_STR_LEN];		// 1 byte * SETTING_STR_LEN
	char valueStr[SETTING_VAL_LEN];		// 1 byte * SETTING_VAL_LEN
	int filler;				// 4 bytes
	settingEnum settingType;		// 1 byte, padded to 4
} UI_setting;					// 8 total + standard


const char UISettingNames[SETTING_NUM][SETTING_STR_LEN] = {
	"Power ",
	"Phase ",
	"Phase EN",
	"Step ",
	"Entry Mode",
	"Clk 0 En",
	"Clk 1 En",
	"Clk 2 En",
	"Inv Clk 0",
	"Inv Clk 1",
	"Inv Clk 2"
};

struct UISettings {
	UI_powerSetting *powerOutput;
	UI_intSetting *phaseOffset;
	UI_boolSetting *phaseEnable;
	UI_intSetting *stepValue;
	UI_intSetting *entryMode;
	UI_boolSetting *clk0Enable;
	UI_boolSetting *clk1Enable;
	UI_boolSetting *clk2Enable;
	UI_boolSetting *clk0Invert;
	UI_boolSetting *clk1Invert;
	UI_boolSetting *clk2Invert;
	uint8_t currentIndex;
};


struct ClkStruct {
	bool clk_enabled;
	char clkName[5] = "CLK";
	uint32_t clk_value = 7000000;
};

// Cursor pos: 0-9, 0-6 is clock digits, 7-9 is clock selections
struct UI {
	ClkStruct* currentClk;
	uint8_t cursorPos;
	bool editMode;
	uint8_t currentClkIndex;
	displayState state;
	bool update_clocks;
	UISettings *settings;
};


// Places an 32bit integer in EEPROM
void UIStoreInt(uint32_t page_num, uint8_t offset, int32_t int_value);

// Places a setting struct in EEPROM as a page.
void UIStoreSetting(uint8_t setting_index, UI_setting *setting);

// Retrieves an integer from EEPROM
uint32_t UIRestoreInt(uint32_t page_num, uint8_t offset);

// Restores a setting struct from EEPROM
UI_setting UIRestoreSetting(uint8_t setting_index);

void UIInit();
void UITick();
void UIForm();
void UIDrawVFO();
void UIDrawSettings();
void UIHandleSettingsEvents();
void UIHandleVFOEvents();
void UIUpdateClock(bool up);
void UIIntToStr(int numIn, char *outString, int strLen);
void UIDoctorString(long int numIn, char *outString, int strLen);
void UIStepClock(ClkStruct *clock, event inputEvent);
void UIEditClock(ClkStruct *clock, event inputEvent);
uint16_t selectNDigit(long int numIn, int N);
bool changeSetting(UI_setting &setting);
