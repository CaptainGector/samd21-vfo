/*
   eepromSim.h by Zachary Whitlock

   Written for the samd21 VFO, a ham radio project.

	This file contains the declarations for reading/writing
	to the Non-Volatile Memory (NVM).
*/


// The EEPROM on the SAMD21G18 has to be placed in the 256KB of flash memory.
// Other varients like the SAMD21x17 or SAMD21x16 have 1-4KB of RWW memory, 
// unfortunately the 18 series does not. The 18 series does have a LOT
// more flash memory, however.


// Flash memory is organized into rows and pages.
// A row has 4 pages, and a page is 64 bytes.
// There is 256kB of flash memory, 1024 rows, and 4096 pages.
//
// Register	= 4B
// Page		= 64B
// Row		= 256B
// Flash Total 	= 256kB
//
// The equation for finding the address of a page is:
//  PageNum = (RowNum*4)+PagePosInRow
//  PageAddr = (PageNum) * PageSize
// 
// The last row (row 1024) will hold 256 bytes of data.
// Bear in mind that flashing a program to the device usually
// erasing the flash memory, including the area for EEPROM.

#include "Arduino.h"

#ifndef EEPROMSIM_H_INCLUDED
#define EEPROMSIM_H_INCLUDED

#define EEPROM_N_ROWS		1
#define EEPROM_ROW_SIZE		256	// Bytes
#define EEPROM_PAGE_SIZE	64	// Bytes
#define EEPROM_DEFAULT_ROW	1024

// buffers
extern uint32_t pageBuffer[EEPROM_PAGE_SIZE/4]; // divide by 4 since there are 4 bytes per uint32_t
extern uint32_t rowBuffer[EEPROM_ROW_SIZE/4];

// 	Init
// Tell the NVM controller how much EEPROM we're using at the end
// 	Read/write user row to set EEPROM size
// Instantiate stuff
void eepromInit();

//	Copy & Write a Page
// Preserves the data that was in NVM already, and only overwrites 
// necessary stuff
void eepromSafeWritePage(uint32_t row_num, uint32_t page_num, uint32_t *dataSrc);

// 	Read Page
// Reads an entire page from NVM into pageBuffer
void eepromReadPage(uint32_t row_num, uint32_t pageNum);

//	Read Row
// Reads an entire row from NVM into rowBuffer;
void eepromReadRow(uint32_t row_num);

// 	Write Page
// Writes a page to NVM
uint32_t eepromWritePage(uint32_t row_num, uint32_t page_num, uint32_t *dataSrc);

// 	Read Status
// Return the status of the NVM controller
uint32_t eepromStatus();

//	Print Page
// Reads a page and prints it to the serial monitor
// (Debugging feature!)
void eepromPrintPage(uint32_t row_num, uint32_t page_num);

//	Print Row
// Reads a row and prints it to the serial monitor
// (Debugging feature!)
void eepromPrintRow(uint32_t row_num);

//	Erase Row
// Erases a row in memory
void eepromEraseRow(uint32_t row_num);
void eepromCopyPageBuffer(uint32_t *dst, uint32_t len);
void eepromCopyRowBuffer(uint32_t *dst, uint32_t len);
#endif
