#include <I2C_DMAC.h>

// Function prototypes
void si5351_init();
void si5351_setfreq(uint8_t clknum, uint32_t fout, bool invert);
