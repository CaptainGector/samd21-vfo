/*
   ui.cpp by Zachary Whitlock

   Written for the samd21 VFO, a ham radio project.

*/

#include "ui.h"

// Normally declared in the main C++ file.
extern UI UIHandle;
extern ClkStruct clk0;
extern ClkStruct clk1;
extern ClkStruct clk2;


// Internal
ClkStruct *clocks[3];

UI_setting UISettingsArray[SETTING_NUM];
UISettings settings;

// Initilizes the settings and so forth
void UIInit() {
	char setBuf[SETTING_VAL_LEN] = {};

	// Restore clock values from EEPROM
	// (Note that EEPROM should therefor be initiated first)

	uint32_t val0, val1, val2;
	val0 = UIRestoreInt(0, 0);
	val1 = UIRestoreInt(0, 1);
	val2 = UIRestoreInt(0, 2);
	// make sure values haven't been cleared by a programming
	if (val0 < 0xFFFFFFFF)
		clk0.clk_value = val0;
	if (val1 < 0xFFFFFFFF)
		clk1.clk_value = val1;
	if (val2 < 0xFFFFFFFF)
		clk2.clk_value = val2;




	// Struct sanity check
	if(sizeof(UI_intSetting) != sizeof(UI_powerSetting) || sizeof(UI_boolSetting) != sizeof(UI_setting)) {
		SerialUSB.print("Sizeof intSetting: ");
		SerialUSB.println(sizeof(UI_intSetting));
		SerialUSB.print("Sizeof powerSetting: ");
		SerialUSB.println(sizeof(UI_powerSetting));
		SerialUSB.print("Sizeof boolSetting: ");
		SerialUSB.println(sizeof(UI_boolSetting));
		SerialUSB.print("Sizeof Setting: ");
		SerialUSB.println(sizeof(UI_setting));
		SerialUSB.print("Sizeof bool: ");
		SerialUSB.println(sizeof(bool));
		SerialUSB.print("Sizeof enum: ");
		SerialUSB.println(sizeof(powerState));
		SerialUSB.print("Sizeof int: ");
		SerialUSB.println(sizeof(int));
		SerialUSB.print("Sizeof char: ");
		SerialUSB.println(sizeof(char));
		SerialUSB.println("Zack you idiot they don't match.");
	}

	// Set the titles for the clocks
	strcpy(clk0.clkName, "Clk0");
	strcpy(clk1.clkName, "Clk1");
	strcpy(clk2.clkName, "Clk2");

	// Point the clocks array to the right clocks
	clocks[0] = &clk0;
	clocks[1] = &clk1;
	clocks[2] = &clk2;

	// Point the handle at our settings
	UIHandle.settings = &settings;

	// Point the settings at the right locations
	settings.powerOutput = (UI_powerSetting*) &UISettingsArray[0];
	settings.phaseOffset = (UI_intSetting*) &UISettingsArray[1];
	settings.phaseEnable = (UI_boolSetting*) &UISettingsArray[2];
	settings.stepValue = (UI_intSetting*) &UISettingsArray[3];
	settings.entryMode = (UI_intSetting*) &UISettingsArray[4];
	settings.clk0Enable = (UI_boolSetting*) &UISettingsArray[5];
	settings.clk1Enable = (UI_boolSetting*) &UISettingsArray[6];
	settings.clk2Enable = (UI_boolSetting*) &UISettingsArray[7];
	settings.clk0Invert = (UI_boolSetting*) &UISettingsArray[8];
	settings.clk1Invert = (UI_boolSetting*) &UISettingsArray[9];
	settings.clk2Invert = (UI_boolSetting*) &UISettingsArray[10];

	// Set the types for the settings
	SerialUSB.print("Phase #: ");
	SerialUSB.println(S_PHASE);
	settings.powerOutput->settingType = S_POWER;
	settings.phaseOffset->settingType = S_PHASE;
	settings.phaseEnable->settingType = S_PHASE_EN;
	settings.stepValue->settingType = S_STEP;
	settings.entryMode->settingType = S_ENTRY_M;

	settings.clk0Enable->settingType = S_CLK0_EN;
	settings.clk1Enable->settingType = S_CLK1_EN;
	settings.clk2Enable->settingType = S_CLK2_EN;

	settings.clk0Invert->settingType = S_CLK0_INV;
	settings.clk1Invert->settingType = S_CLK1_INV;
	settings.clk2Invert->settingType = S_CLK2_INV;

	// Set the names for the settings
	char name[SETTING_STR_LEN];
	for (int i = 0; i < SETTING_NUM; i++) {
		strcpy(name, (char*)UISettingNames[i]);
		if (name != NULL) {
			strncpy((char*)UISettingsArray[i].name, name, SETTING_STR_LEN);
		}
	}

	// Power
	strcpy((char*)settings.powerOutput->valueStr, "8mA");

	// Phase
	settings.phaseOffset->value = 90;
	UIIntToStr(settings.phaseOffset->value, setBuf, SETTING_VAL_LEN);
	strncpy(settings.phaseOffset->valueStr, setBuf, SETTING_VAL_LEN);

	strcpy((char*)settings.phaseEnable->valueStr, "ON");

	// Clocks
	strcpy((char*)settings.clk0Enable->valueStr, "ON");
	strcpy((char*)settings.clk1Enable->valueStr, "OFF");
	strcpy((char*)settings.clk2Enable->valueStr, "OFF");

	strcpy((char*)settings.clk0Invert->valueStr, "OFF");
	strcpy((char*)settings.clk1Invert->valueStr, "OFF");
	strcpy((char*)settings.clk2Invert->valueStr, "OFF");

	// Step
	settings.stepValue->value = 2500;
	strcpy((char*)settings.stepValue->valueStr, "2.5k");

	// Entry Mode
	settings.entryMode->value = MODE_EDIT;
	strcpy((char*)settings.entryMode->valueStr, "EDIT");

	SerialUSB.print("End result #: ");
	SerialUSB.println(settings.phaseOffset->settingType);
	SerialUSB.print("Array result #: ");
	SerialUSB.println(UISettingsArray[1].settingType);

	// Restore some settings from EEPROM

	// Check if the rows are all 1s (indicates a programming ruining things);
	SerialUSB.println("Checking for settings in EEPROM");
	for (int i = 0; i < SETTING_NUM; i++) {
		UI_setting settingContainer = UIRestoreSetting(i);

		// Check data inside the container
		uint32_t testVal[sizeof(UI_setting)/4];
		memcpy(testVal, &settingContainer, sizeof(UI_setting));

		if (testVal[(sizeof(UI_setting)/4)-1] == 0xFFFFFFFF) {
			// Skip
			continue;
		} else {
			memcpy(&UISettingsArray[i], &settingContainer, sizeof(UI_setting));
		}
	}

	UIHandle.state = VFO_PAGE;
}

void UITick()
{
	return;
} // end UITick()

// Handles the drawing of the UI.
void UIForm()
{
	if (UIHandle.state == SETTINGS_PAGE) {
		UIHandleSettingsEvents();
		UIDrawSettings();
	} else if (UIHandle.state == VFO_PAGE) {
		UIHandleVFOEvents();
		UIDrawVFO();
	}

} // end UIForm();

void UIUpdateClock(bool up)
{
	return;
} // end UIUpdateClock();

void UIDrawSettings() {
	// For up to 4 settings, display each on the screen with their value
	// +---------------------------------+
	// | Setting 1           | Value     |
	// +---------------------------------+
	// | Setting 2           | Value     |
	// +---------------------------------+
	// | Setting 3           | Value     |
	// +---------------------------------+
	// | Setting 4           | Value     |
	// +---------------------------------+
	// Scrolling
	// Show 4 values starting from N index
	// N index incremented by 1 if selected index is greater than shown
	// Allowable indices stop at number of settings - 4
	uint8_t subIndex;
	uint8_t offsetIndex = 0;
	if (settings.currentIndex >= 4) {
		offsetIndex = settings.currentIndex - 3;
	}

	// This draws 4 setting entries
	for (subIndex = 0; subIndex < 4; subIndex++) {
		displaySetCursor(SETTING_X_MARGIN, SETTING_V_SPACING*(subIndex+1)-11);
		if (settings.currentIndex == subIndex + offsetIndex) {
			displayFillArea(0, SETTING_V_SPACING*(subIndex), SETTING_V_LINE_X, SETTING_V_SPACING*(subIndex+1), true);
			displayDrawString(UISettingsArray[subIndex+offsetIndex].name, Font_7x10, false);

			// Value
			// Setting value FILL if edit mode
			displaySetCursor(SETTING_V_LINE_X+7, SETTING_V_SPACING*(subIndex+1)-11);
			if (UIHandle.editMode)
				displayFillArea(SETTING_V_LINE_X, SETTING_V_SPACING*(subIndex), 128, SETTING_V_SPACING*(subIndex+1), true);
			displayDrawString(UISettingsArray[subIndex+offsetIndex].valueStr, Font_7x10, !UIHandle.editMode);
		} else {
			displayDrawString(UISettingsArray[subIndex+offsetIndex].name, Font_7x10, true);
			displaySetCursor(SETTING_V_LINE_X+7, SETTING_V_SPACING*(subIndex+1)-11);
			// Setting value
			displayDrawString(UISettingsArray[subIndex+offsetIndex].valueStr, Font_7x10, true);
		}
		displayFillArea(0, SETTING_V_SPACING*(subIndex+1), 128, SETTING_V_SPACING*(subIndex+1)+1, true);
	}
	displayFillArea(SETTING_V_LINE_X, 0, SETTING_V_LINE_X+1, 64, true);
}

void UIHandleSettingsEvents() {
	if (UIHandle.editMode) {
		// Will return FALSE if it was a PRESS event
		// Note: this function call results in the settings
		// actually being changed. Real obvious, yeh?
		if (changeSetting(UISettingsArray[settings.currentIndex])){
			return;
		} else {
			// Save settings every time we leave edit mode
			UIStoreInt(0, 0, clk0.clk_value);	 // Save clock values
			UIStoreInt(0, 1, clk1.clk_value);
			UIStoreInt(0, 2, clk2.clk_value);

			// Save the current setting (the entire struct)
			UIStoreSetting(settings.currentIndex, &UISettingsArray[settings.currentIndex]);


			// Toggle edit mode
			UIHandle.editMode = !UIHandle.editMode;
			return;
		}

	}


	// Handle encoder events
	event recentEvent = EventRead();
	if (recentEvent == NO_EVENT)
		return;

	if (recentEvent == TURN_RIGHT && settings.currentIndex < SETTING_NUM-1 && !UIHandle.editMode) {
		settings.currentIndex++;
	} else if (recentEvent == TURN_LEFT && !UIHandle.editMode) {
		settings.currentIndex--;
	} else if (recentEvent == PRESS) {
		// Toggle edit mode
		UIHandle.editMode = !UIHandle.editMode;
	} else if (recentEvent == HELD_TURN) {
		UIHandle.state = VFO_PAGE;
		wipeDisplay();
	}

	// Reset Index if out of bounds
	if (settings.currentIndex > SETTING_NUM)
		settings.currentIndex = 0;

}

void UIHandleVFOEvents() {
	event recentEvent = EventRead();
	if (recentEvent == NO_EVENT)
		return;

		if (settings.entryMode->value == MODE_STEP) {
			if (recentEvent == HELD_TURN) {
				UIHandle.state = SETTINGS_PAGE;
				wipeDisplay();
			} else if (recentEvent == TURN_LEFT || recentEvent == TURN_RIGHT) {
				UIStepClock(UIHandle.currentClk, recentEvent);
			} else if (recentEvent == HELD) {
				// Goto next clock
				UIHandle.currentClkIndex++;
				if (UIHandle.currentClkIndex >= UI_CLOCKS)
				UIHandle.currentClkIndex = 0;

				UIHandle.currentClk = clocks[UIHandle.currentClkIndex];
			}
		} else if (settings.entryMode->value == MODE_EDIT) {
			if (recentEvent == HELD_TURN) {
				UIHandle.state = SETTINGS_PAGE;
				wipeDisplay();
			} else if (recentEvent == PRESS) {
				// Add to cursor
				UIHandle.cursorPos++;

				// Prevent overrun
				if (UIHandle.cursorPos > 6)
					UIHandle.cursorPos = 0;

			} else if (recentEvent == TURN_LEFT || recentEvent == TURN_RIGHT) {
				UIEditClock(UIHandle.currentClk, recentEvent);
			} else if (recentEvent == HELD) {
				// Goto next clock
				UIHandle.currentClkIndex++;
				if (UIHandle.currentClkIndex >= UI_CLOCKS)
				UIHandle.currentClkIndex = 0;

				UIHandle.currentClk = clocks[UIHandle.currentClkIndex];
			}

		}

}


void UIDrawVFO() {

	// ╔══════════════════════════════════════╗
	// ║░░░░░░░░░░░░░░CLK 0░░░░░░░░░░░░░░░░░░░║
	// ║░╔═══╦═══╗╔═══╦╗░╔╦═══╗╔═══╦═══╦═══╗░░║
	// ║░║╔═╗║╔═╗║║╔═╗║║░║║╔═╗║║╔═╗║╔═╗║╔═╗║░░║
	// ║░║║║║╠╝╔╝║╚╝╔╝║╚═╝╠╝╔╝║║║║║║║║║║║║║║░░║
	// ║░║║║║║░║╔╝░░║╔╩══╗║░║╔╝║║║║║║║║║║║║║░░║
	// ║░║╚═╝║░║║╔╗░║║░░░║║░║║╔╣╚═╝║╚═╝║╚═╝║░░║
	// ║░╚═══╝░╚╝╚╝░╚╝░░░╚╝░╚╝╚╩═══╩═══╩═══╝░░║
	// ║░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░║
	// ║░░░░CLK 1   10.101.000      HZ░░░░░░░░║
	// ║░░░░CLK 2   30.300.101      HZ░░░░░░░░║
	// ╚══════════════════════════════════════╝


	char clkMainStr[UI_VFO_CHARS];
	char clk1Str[UI_VFO_CHARS];
	char clk2Str[UI_VFO_CHARS];


	// Calculate the indices of the other two clocks based on the active one.
	// sorry about the spagetti code - it's essentially just wrapping the number
	// back to 0 after it passes the number of clocks.
	int index2 = (UIHandle.currentClkIndex + 1 >= UI_CLOCKS) ? (UIHandle.currentClkIndex+1)-UI_CLOCKS : UIHandle.currentClkIndex+1;
	int index3 = (UIHandle.currentClkIndex + 2 >= UI_CLOCKS) ? (UIHandle.currentClkIndex+2)-UI_CLOCKS : UIHandle.currentClkIndex+2;

	UIDoctorString(UIHandle.currentClk->clk_value, clkMainStr, UI_VFO_CHARS);
	UIDoctorString(clocks[index2]->clk_value, clk1Str, UI_VFO_CHARS);
	UIDoctorString(clocks[index3]->clk_value, clk2Str, UI_VFO_CHARS);

	displaySetCursor(VFO_X_MARGIN, VFO_ACTIVE_Y_POS);

	// Draw the active clock value
	displayDrawString(clkMainStr, Font_11x18, true);

	// Place clock title above main clock
	displaySetCursor(64-(6*2), VFO_ACTIVE_Y_POS-8-1);
	displayDrawString((char *)UIHandle.currentClk->clkName, Font_6x8, true);

	//displaySetCursor(104, VFO_ACTIVE_Y_POS+9);
	//displayDrawString((char*)"Hz", Font_7x10, true);

	displaySetCursor(0, VFO_Y_DIST + VFO_ACTIVE_Y_POS);
	displayDrawString(clocks[index2]->clkName, Font_6x8, true);

	displaySetCursor(30, VFO_Y_DIST + VFO_ACTIVE_Y_POS);
	displayDrawString(clk1Str, Font_6x8, true);

	displaySetCursor(12*8+2, VFO_Y_DIST + VFO_ACTIVE_Y_POS);
	displayDrawString((char*)"Hz", Font_6x8, true);

	displaySetCursor(0, VFO_Y_DIST + VFO_ACTIVE_Y_POS + 10);
	displayDrawString(clocks[index3]->clkName, Font_6x8, true);

	displaySetCursor(30, VFO_Y_DIST + VFO_ACTIVE_Y_POS + 10);
	displayDrawString(clk2Str, Font_6x8, true);

	displaySetCursor(12*8+2, VFO_Y_DIST + VFO_ACTIVE_Y_POS + 10);
	displayDrawString((char*)"Hz", Font_6x8, true);

	// Draw the cursor underneath the big letters
	if (settings.entryMode->value == MODE_EDIT) {
		uint16_t n = UIHandle.cursorPos;
		if (UIHandle.cursorPos >= 3)
			n++;
		if (UIHandle.cursorPos >= 6)
			n++;

		displayDrawLine(	(VFO_X_MARGIN + 11*(sizeof(clkMainStr)-1)) - 11*n,
											VFO_ACTIVE_Y_POS + 18,
											(VFO_X_MARGIN + 11*(sizeof(clkMainStr)-1)) - 11*(n+1),
											VFO_ACTIVE_Y_POS + 18,
											true);

	}

	return;
}

// Modify a setting
bool changeSetting(UI_setting &setting) {
	char valueStr[SETTING_VAL_LEN] = {};
	settingEnum sType = setting.settingType;
	event inputEvent = EventRead();

	if (inputEvent == PRESS) {
		return false;
	}

	//SerialUSB.println(setting.valueStr);
	//SerialUSB.println(sType);

	// Check setting based on name
	if (sType == S_POWER) { // POWER
		// Cycle through enums
		// Barf. I don't like this at all lol
		if (inputEvent == TURN_LEFT) {
			switch (settings.powerOutput->value) {
				case POWER_2MA:
					strcpy(settings.powerOutput->valueStr, "4mA");
					settings.powerOutput->value = POWER_4MA;
					break;
				case POWER_4MA:
					strcpy(settings.powerOutput->valueStr, "6mA");
					settings.powerOutput->value = POWER_6MA;
					break;
				case POWER_6MA:
					strcpy(settings.powerOutput->valueStr, "8mA");
					settings.powerOutput->value = POWER_8MA;
					break;
				case POWER_8MA:
					strcpy(settings.powerOutput->valueStr, "2mA");
					settings.powerOutput->value = POWER_2MA;
					break;
				default:
					settings.powerOutput->value = POWER_4MA;
			}
		}
		if (inputEvent == TURN_RIGHT) {
			switch (settings.powerOutput->value) {
				case POWER_2MA:
					strcpy(settings.powerOutput->valueStr, "8mA");
					settings.powerOutput->value = POWER_8MA;
					break;
				case POWER_4MA:
					strcpy(settings.powerOutput->valueStr, "2mA");
					settings.powerOutput->value = POWER_2MA;
					break;
				case POWER_6MA:
					strcpy(settings.powerOutput->valueStr, "4mA");
					settings.powerOutput->value = POWER_4MA;
					break;
				case POWER_8MA:
					strcpy(settings.powerOutput->valueStr, "6mA");
					settings.powerOutput->value = POWER_6MA;
					break;
				default:
					settings.powerOutput->value = POWER_4MA;
			}
		}
	} else if (sType == S_PHASE) { // PHASE
		//UI_intSetting *phase = (UI_intSetting*) &setting;
		if (inputEvent == TURN_LEFT) {
			settings.phaseOffset->value++;
		} else if (inputEvent == TURN_RIGHT) {
			settings.phaseOffset->value--;
		}
		UIIntToStr(settings.phaseOffset->value, valueStr, 2);
		strcpy(settings.phaseOffset->valueStr, valueStr);
	} else if (sType == S_PHASE_EN) { // PHASE EN
		// Turn on/off
		if (inputEvent == TURN_LEFT || inputEvent == TURN_RIGHT) {
			settings.phaseEnable->value = !settings.phaseEnable->value;
			if (settings.phaseEnable->value) {
				strcpy(settings.phaseEnable->valueStr, "ON");
			} else {
				strcpy(settings.phaseEnable->valueStr, "OFF");
			}
		}
	} else if (sType == S_ENTRY_M) {
		// Change entry mode
		if (inputEvent == TURN_LEFT || inputEvent == TURN_RIGHT) {
			if (settings.entryMode->value == MODE_EDIT){
				settings.entryMode->value = MODE_STEP;
				strcpy(settings.entryMode->valueStr, "STEP");
			} else {
				settings.entryMode->value = MODE_EDIT;
				strcpy(settings.entryMode->valueStr, "EDIT");
			}
		}
	} else if (sType == S_CLK0_EN) { // CLK 0 ENABLE
		if (inputEvent == TURN_LEFT || inputEvent == TURN_RIGHT) {
			settings.clk0Enable->value = !settings.clk0Enable->value;
			if (settings.clk0Enable->value) {
				strcpy(settings.clk0Enable->valueStr, "ON");
			} else {
				strcpy(settings.clk0Enable->valueStr, "OFF");
			}
		}
	} else if (sType == S_CLK1_EN) { // CLK 1 ENABLE
		if (inputEvent == TURN_LEFT || inputEvent == TURN_RIGHT) {
			settings.clk1Enable->value = !settings.clk1Enable->value;
			if (settings.clk1Enable->value) {
				strcpy(settings.clk1Enable->valueStr, "ON");
			} else {
				strcpy(settings.clk1Enable->valueStr, "OFF");
			}
		}
	} else if (sType == S_CLK2_EN) { // CLK 2 ENABLE
		if (inputEvent == TURN_LEFT || inputEvent == TURN_RIGHT) {
			settings.clk2Enable->value = !settings.clk2Enable->value;
			if (settings.clk2Enable->value) {
				strcpy(settings.clk2Enable->valueStr, "ON");
			} else {
				strcpy(settings.clk2Enable->valueStr, "OFF");
			}
		}
	} else if (sType == S_CLK0_INV) { // CLK 0 INVERT
		if (inputEvent == TURN_LEFT || inputEvent == TURN_RIGHT) {
			settings.clk0Invert->value = !settings.clk0Invert->value;
			if (settings.clk0Invert->value) {
				strcpy(settings.clk0Invert->valueStr, "ON");
			} else {
				strcpy(settings.clk0Invert->valueStr, "OFF");
			}
		}
	} else if (sType == S_CLK1_INV) { // CLK 1 INVERT
		if (inputEvent == TURN_LEFT || inputEvent == TURN_RIGHT) {
			settings.clk1Invert->value = !settings.clk1Invert->value;
			if (settings.clk1Invert->value) {
				strcpy(settings.clk1Invert->valueStr, "ON");
			} else {
				strcpy(settings.clk1Invert->valueStr, "OFF");
			}
		}
	} else if (sType == S_CLK2_INV) { // CLK 2 INVERT
		if (inputEvent == TURN_LEFT || inputEvent == TURN_RIGHT) {
			settings.clk2Invert->value = !settings.clk2Invert->value;
			if (settings.clk2Invert->value) {
				strcpy(settings.clk2Invert->valueStr, "ON");
			} else {
				strcpy(settings.clk2Invert->valueStr, "OFF");
			}
		}
	} else if (sType == S_STEP) { // STEP
		if (inputEvent == TURN_LEFT) {
			settings.stepValue->value -= 100;
		} else if (inputEvent == TURN_RIGHT) {
			settings.stepValue->value += 100;
		}

		if (settings.stepValue->value < 0)
			settings.stepValue->value = 0;
		if (settings.stepValue->value > SETTING_STEP_MAX)
			settings.stepValue->value = SETTING_STEP_MAX;

		settings.stepValue->valueStr[0] = '0' + selectNDigit(settings.stepValue->value, 3);
		settings.stepValue->valueStr[1] = '.';
		settings.stepValue->valueStr[2] = '0' + selectNDigit(settings.stepValue->value, 2);
		settings.stepValue->valueStr[3] = 'k';
		settings.stepValue->valueStr[4] = 0;
		SerialUSB.println(settings.stepValue->value);

	} else if (sType == S_ENTRY_M) { // ENTRY MODE

	}

	return true;
}

// Increment or decrement the given clock by the current step value
void UIStepClock(ClkStruct *clock, event inputEvent) {
	if (inputEvent == TURN_LEFT) {
		clock->clk_value += settings.stepValue->value;
	} else if (inputEvent == TURN_RIGHT) {
		clock->clk_value -= settings.stepValue->value;
	}
}

// Increment or decrement the given clock based on the cursor position
void UIEditClock(ClkStruct *clock, event inputEvent) {
	SerialUSB.print("Cursor: ");
	SerialUSB.println(UIHandle.cursorPos);

	uint32_t incrValue;
	incrValue = pow(10, UIHandle.cursorPos);
	if (inputEvent == TURN_LEFT) {
		clock->clk_value += incrValue;
		if (clock->clk_value > 99999999)
			clock->clk_value = 99999999;
	} else if (inputEvent == TURN_RIGHT) {
		clock->clk_value -= incrValue;
		if (clock->clk_value > 99999999)
			clock->clk_value = 99999999;
	}
}


// Simply converts integer to string
void UIIntToStr(int numIn, char *outString, int strLen) {
	for (int i = strLen; i >= 0; i--) {
		outString[strLen-i] = '0' + selectNDigit(numIn, i);
	}
}


// Grab value from EEPROM


//	INTO EEPROM
// Anything under 4 bytes is padded
// Anything over 4 bytes is cut

// Places an entire UI_Setting into EEPROM
// (I guess making them all the same size was smurt after al)
void UIStoreSetting(uint8_t setting_index, UI_setting *setting) {

	//SerialUSB.print("Saving index: ");
	//SerialUSB.print(setting_index);
	//SerialUSB.print(", ");
	//SerialUSB.println(setting->name);

	// Verify that we're under 64B
	if (sizeof(UI_setting) > 64) {
		// For now, I'd rather this works oddly than simply not doing anything.
		//return;
	}

	// Allocate memory for the structure copy
	// This should just about always be 16 bits
	// (Unless you use something greater or larger than 32bit)
	uint32_t dataBuffer[16];

	// Zero out memory
	memset(dataBuffer, 0, 16);

	// Fill memory with copy of the UI_setting
	memcpy(dataBuffer, setting, sizeof(UI_setting));

	// Figure out which page to place this in.
	uint16_t row_num = UI_EEPROM_ROW + uint16_t(setting_index/4) + 1;
	uint16_t page_num = (setting_index % 4);

	// Save the data to memory
	eepromSafeWritePage(row_num, page_num, dataBuffer);
}

// Places an 32bit integer in EEPROM
void UIStoreInt(uint32_t page_num, uint8_t offset, int32_t int_value) {
	// Short term buffer
	uint32_t dataBuffer[16] = {};

	// Fill buffer with old data
	eepromReadPage(UI_EEPROM_ROW, page_num);
	eepromCopyPageBuffer(dataBuffer, 16);

	// Replace at offset
	dataBuffer[offset] = int_value; // pageBuffer is defined in the eeprom library TODO: change name?

	// Place into EEPROM
	eepromSafeWritePage(UI_EEPROM_ROW, page_num, dataBuffer);
}

// Places a boolean in EEPROM
void UIStoreBool(uint32_t page_num, uint8_t offset,  bool bool_value) {
	// Short term buffer
	uint32_t dataBuffer[16] = {};

	// Fill buffer with old data
	eepromReadPage(UI_EEPROM_ROW, page_num);
	eepromCopyPageBuffer(dataBuffer, 16);

	// Replace at offset
	dataBuffer[offset] = bool_value; // pageBuffer is defined in the eeprom library TODO: change name?

	// Place into EEPROM
	eepromSafeWritePage(UI_EEPROM_ROW, page_num, dataBuffer);
}

// Places an enum in EEPROM
// (Enums are 1 byte)
void UIStoreEnum(uint32_t page_num, uint8_t offset,  uint8_t enum_value) {
	// Pad to 2 bytes
}

// 	OUT OF EEPROM

// Restores a UI setting from EEPROM
UI_setting UIRestoreSetting(uint8_t setting_index) {
	UI_setting bufferStruct;

	// Figure out where this index referes to
	uint16_t row_num = UI_EEPROM_ROW + uint16_t(setting_index / 4) + 1;
	uint16_t page_num = (setting_index % 4);

	// Read entire page and copy enough bytes back into buffer struct to fill it
	eepromReadPage(row_num, page_num);

	// Copy data from page read into struct
	memcpy(&bufferStruct, pageBuffer, sizeof(UI_setting));

	// Return the struct
	return bufferStruct;
}


// Retrieves an integer from EEPROM
uint32_t UIRestoreInt(uint32_t page_num, uint8_t offset) {
	// Read page
	eepromReadPage(UI_EEPROM_ROW, page_num);

	// Return data at offset
	return pageBuffer[offset];
}

// Creates a N digit string from an integer.
void UIDoctorString(long int numIn, char *outString, int strLen) {
	// Make 3 sets of strings for each segment of string
	char str1[4] = "00.";
	char str2[5] = "000.";
	char str3[4] = "000";
	// remember that C-strings include the tailing null-byte in size.

	// Start at the beginning of the string and work backwards until 0
	// the -3 is for the periods and null byte
	int currIndex = strLen-3;
	int count;

	// fill the first string
	for (count = 0; count < 2; count++) {
		str1[count] = '0' + selectNDigit(numIn, currIndex);
		currIndex--;
	}
	// Second
	for (count = 0; count < 3; count++) {
		str2[count] = '0' + selectNDigit(numIn, currIndex);
		currIndex--;
	}
	// Third
	for (count = 0; count < 2; count++) {
		str3[count] = '0' + selectNDigit(numIn, currIndex);
		currIndex--;
	}

	// Assemble final output string
	strncpy(outString, str1, 3);
	strncpy(outString+3, str2, 4);
	strncpy(outString+7, str3, 3);


}

uint16_t selectNDigit(long int numIn, int N) {
	// This operates on integers only.
	// The algorithm selects N digit - that's the complicated mess
	uint16_t result;
	result = ((numIn % (int)pow(10, N+1)) - (numIn % (int)pow(10, N)))/((int)pow(10, N));
	return result;
}
