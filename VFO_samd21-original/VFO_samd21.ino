/*  Title: SAMD21_VFO Main Program, Author: Zachary Whitlock

    Display code is largely taken from: https://github.com/afiskon/stm32-ssd1306,
    and https://github.com/adafruit/Adafruit_SSD1306.
*/

// Keep in mind these are probably ordered specifically ;)
#include "si5351.h"
#include "eventFIFO.h"
#include "display.h"
#include "ui.h"
#include "eepromSim.h"
#include <I2C_DMAC.h>

//#define TRACE_BUFFER_SIZE 256
//__attribute__((__aligned__(TRACE_BUFFER_SIZE * sizeof(uint32_t)))) uint32_t mtb[TRACE_BUFFER_SIZE];

#define DEBUG false

#define ENCODER_ACCEL_MAX 10000

// In loop ticks, not a real time unit.
#define HELD_TIME 10
// Time in seconds between auto-saving the clocks
#define SAVE_TIME 20

//#include "i2c_handler.h"
//#include "fonts.h"

// Timing variables
long int lastUpdate;
uint32_t saveTime;

volatile long int value = 7100000;
volatile int deltaValue = 0;
volatile int lastEncoded = 0;

// Encoder related
volatile uint8_t lastState = 0;
volatile uint8_t expectedValue = 5;
unsigned int encoderAccel = 1; // should never drop below 1
volatile bool encoderDebounce = false;

volatile byte stateHistory = 0x0000;

UI UIHandle;
ClkStruct clk0;
ClkStruct clk1;
ClkStruct clk2;

// Used in the si5351.cpp file as well.
uint8_t  si5351bx_drive[3] = {3, 3, 3}; // 0=2ma 1=4ma 2=6ma 3=8ma for CLK 0,1,2

// Event processing variables
volatile int downTime;
volatile bool buttonWas;
volatile bool buttonDown; // is declared in fifo as well

void setup() {
	// Setup watchdog timer to reset after a certain timeout


	clk0.clk_enabled = true;
	clk1.clk_enabled = true;
	clk2.clk_enabled = false;

	clk0.clk_value = 7100000;
	clk1.clk_value = 7100000;
	clk2.clk_value = 14148000;

	UIHandle.currentClk = &clk0;
	UIHandle.currentClkIndex = 0;


	//pinMode(4, INPUT_PULLUP);

	if (DEBUG) {
		SerialUSB.begin(57600);
		while (!SerialUSB);                 // Uncomment if MUST wait for serial bus (serial monitor MUST be open)
		SerialUSB.println("Initilized.");
	}

	// Set up eeprom controller & Run some tests
	eepromInit();
	// Read clock values (hopefully)
	eepromPrintRow(1015);

	// Set up UI
	UIInit();

	//startI2C();
	I2C.begin(400000, REG_ADDR_8BIT, PIO_SERCOM_ALT);            // Start at 400kHz

	delay(100);
	// ------------------------------------------ //
	/*
	   SET UP THE SI5351
	 */
	si5351_init();
	// ------------------------------------------ //

	// ------------------------------------------ //
	/*
	   SET UP ENCODER
	 */
	init_encoder();

	//pinMode(ENC_BTN_PIN, INPUT_PULLUP);
	// ------------------------------------------ //

	//setupTimers();

	// Set up Timer for debouncing
	init_debouncer();

	delay(100);
	bool result;

	result = initSSD1306(); // Should show some noise on the display.
	if (!result) {
		if (DEBUG) SerialUSB.println("Malloc fail!.");
		while (true);
	}
	if (DEBUG) SerialUSB.println("Malloc success.");

	delay(100);
	if (DEBUG) SerialUSB.println("Pushing display full");
	fillDisplay();
	pushDisplay();
	delay(100);
	wipeDisplay();
	pushDisplay();

	delay(100);
	// Write a string to display.
	char error = displayDrawString((char*)"Jesus is King!", Font_7x10, true);
	pushDisplay();
	delay(500);
	//si5351_setfreq(0, 7100000);
}

void loop() {
	// Set clocks once every 20ms
	if (millis() - lastUpdate >= 20) {
		if (clk0.clk_enabled) {
			si5351_setfreq(0, clk0.clk_value, UIHandle.settings->clk0Invert->value);
		} else {
			si5351_setfreq(0, 0, UIHandle.settings->clk0Invert->value);
		}
		if (clk1.clk_enabled) {
			si5351_setfreq(1, clk1.clk_value, UIHandle.settings->clk1Invert->value);
		} else {
			si5351_setfreq(1, 0, UIHandle.settings->clk1Invert->value);
		}
		if (clk2.clk_enabled) {
			si5351_setfreq(2, clk2.clk_value, UIHandle.settings->clk2Invert->value);
		} else {
			si5351_setfreq(2, 0, UIHandle.settings->clk2Invert->value);
		}

		switch (UIHandle.settings->powerOutput->value) {
			case POWER_8MA:
				si5351bx_drive[0] = 3;
				si5351bx_drive[1] = 3;
				si5351bx_drive[2] = 3;
				break;
			case POWER_6MA:
				si5351bx_drive[0] = 2;
				si5351bx_drive[1] = 2;
				si5351bx_drive[2] = 2;
				break;
			case POWER_4MA:
				si5351bx_drive[0] = 1;
				si5351bx_drive[1] = 1;
				si5351bx_drive[2] = 1;
				break;
			default:
				si5351bx_drive[0] = 0;
				si5351bx_drive[1] = 0;
				si5351bx_drive[2] = 0;
		};

		clk0.clk_enabled = UIHandle.settings->clk0Enable->value;
		clk1.clk_enabled = UIHandle.settings->clk1Enable->value;
		clk2.clk_enabled = UIHandle.settings->clk2Enable->value;

		lastUpdate = millis();
	}

	// Save clocks every now and then
	if (millis() - saveTime > SAVE_TIME*1000) {
		UIStoreInt(0, 0, clk0.clk_value);	 // Save clock values
		UIStoreInt(0, 1, clk1.clk_value);
		UIStoreInt(0, 2, clk2.clk_value);

		if (DEBUG) SerialUSB.println("Autosaving clocks...");
		saveTime = millis();
	}

	wipeDisplay();

	UITick();
	UIForm();

	/*
	   displaySetCursor(20, 35);
	   stringOne = (String)freeMemory();//UIHandle.currentClk->clk_value;
	   stringOne.toCharArray(charBuf, 100);
	   displayDrawString(charBuf, Font_7x10, true);
	 */


	pushDisplay();

	//if (DEBUG) SerialUSB.println(UIHandle.cursorPos);
	//if (DEBUG) SerialUSB.println(UIHandle.state);
	//if (DEBUG) SerialUSB.println(UIHandle.currentClk->clk_value);
	//if (DEBUG) SerialUSB.print("Button state: ");
	//if (DEBUG) SerialUSB.println(UIHandle.button_state);

	// Count time down if down
	if (buttonDown) {
		downTime++;
	}

	// If button is up but was down before, process events.
	if (!buttonDown && buttonWas) {
		if (DEBUG) SerialUSB.print("Button down time: ");
		if (DEBUG) SerialUSB.println(downTime);
		if (downTime > HELD_TIME) {
			if (DEBUG) SerialUSB.println("ADDING HELD");
			EventWrite(HELD);
		} else {
			if (DEBUG) SerialUSB.println("ADDING PRESS");
			EventWrite(PRESS);
		}
		downTime = 0;
	}

	buttonWas = buttonDown;
	delay(10);
}

// encoder handler
void EIC_Handler(void)
{
	bool EXINT5 = EIC->INTFLAG.bit.EXTINT5;
	bool EXINT7 = EIC->INTFLAG.bit.EXTINT7;

	EIC->INTFLAG.bit.EXTINT5 = 0;
	EIC->INTFLAG.bit.EXTINT7 = 0;
	EIC->INTFLAG.bit.EXTINT9 = 0;

	// Do nothing for a tiny amount of time to let edges settle
	//int c = 0;
	//while (c < 500) {
	//	c++;
	//}
	uint8_t encState = 0;

	if (encoderDebounce) {
		return;
	}

	encState = (!digitalRead(8) << 1) | !digitalRead(9);
	if (DEBUG) SerialUSB.print("ENC STATE: ");
	if (DEBUG) SerialUSB.println(encState, BIN);


	if (EIC->INTFLAG.bit.EXTINT9) {
		// digitalRead LOW is an ACTIVE state, or a PRESSED state
		// On the SEEDUINO, d7 is PB9

		// This doesn't always seem to change state when this fires.
		// That would imply that you should have an additional delay
		// after the EDGE change. This could be done with another timer
		// or by multi-tasking timer-counter 3.
		//buttonDown = !digitalRead(7);

    //TC3->COUNT16.CTRLA.reg |= TC_CTRLA_ENABLE;
		EIC->INTFLAG.bit.EXTINT9 = 0;
		//encoderDebounce = true;
	}

	// EXTINT7 represents pin 7, d8
	// EXTINT5 represents pin 5, d9

	// a 1 represents an edge occured

	if (EXINT5 || EXINT7) {
		// Update state history
		stateHistory = ((stateHistory << 2) | encState);
		if (DEBUG) SerialUSB.print("State History: ");
		if (DEBUG) SerialUSB.println(stateHistory, BIN);

		// Check if our current state history means we should tick forward
		if (stateHistory == 0b01111100 || stateHistory == 0b01111000) {
			if (DEBUG) SerialUSB.println("ADDING TURN RIGHT");
			EventWrite(TURN_RIGHT);
		} else if (stateHistory == 0b10110100 || stateHistory == 0b10100100) {
			if (DEBUG) SerialUSB.println("ADDING TURN LEFT");
			EventWrite(TURN_LEFT);
		}
		// Check forward
		//uint8_t sM = 0; // states merged

		/*
		sM = (lastState << 2) | encState;
		if (sM == 0b0010 || sM == 0b1011 || sM == 0b1101 || sM == 0b0100) {
			if (DEBUG) SerialUSB.println("ADDING TURN LEFT");
			EventWrite(TURN_LEFT);
		} else if (sM == 0b0001 || sM == 0b0111 || sM == 0b1110 || sM == 0b1000) {
			if (DEBUG) SerialUSB.println("ADDING TURN RIGHT");
			EventWrite(TURN_RIGHT);
		}*/

    //TC3->COUNT16.CTRLA.reg |= TC_CTRLA_ENABLE;
		encoderDebounce = true;
	}

}


void TC3_Handler (void) {
	TC3->COUNT16.INTFLAG.bit.MC0 = 1; //Writing a 1 to INTFLAG.bit.MC0 clears

	// Disable timer again
	//TC3->COUNT16.CTRLA.reg ^= TC_CTRLA_ENABLE;
	//the flag

	// Check button state
	buttonDown = !digitalRead(7);

	encoderDebounce = false;
}


void init_debouncer() {
    // Enable and configure generic clock generator 4
    // Note that generators 0-3 are used by the IDE
    GCLK->GENCTRL.reg = GCLK_GENCTRL_IDC |  // Improve duty cycle
        GCLK_GENCTRL_GENEN |                // Enable generic clock gen
        GCLK_GENCTRL_SRC_OSC8M |          // Select 8MHz as source
        GCLK_GENCTRL_ID(4);                 // Select GCLK4
    while (GCLK->STATUS.bit.SYNCBUSY);      // Wait for synchronization

    // Set clock divider of 1 to generic clock generator 4
    GCLK->GENDIV.reg = GCLK_GENDIV_DIV(1) | // Divide 8 MHz by 1
        GCLK_GENDIV_ID(4);                  // Apply to GCLK4 4
    while (GCLK->STATUS.bit.SYNCBUSY);      // Wait for synchronization

    // Enable GCLK4 and connect it to TCC2 and TC3
    GCLK->CLKCTRL.reg = GCLK_CLKCTRL_CLKEN |    // Enable generic clock
        GCLK_CLKCTRL_GEN_GCLK4 |                // Select GCLK4
        GCLK_CLKCTRL_ID_TCC2_TC3;               // Feed GCLK4 to TCC2/TC3
    while (GCLK->STATUS.bit.SYNCBUSY);          // Wait for synchronization

    TC3->COUNT16.CTRLA.bit.ENABLE = 0; // disable tc3 to change settings
    while (TC3->COUNT16.STATUS.bit.SYNCBUSY);

    TC3->COUNT16.CTRLA.reg |= TC_CTRLA_MODE_COUNT16; //select 16-bit mode
    while (TC3->COUNT16.STATUS.bit.SYNCBUSY);

    TC3->COUNT16.CTRLA.reg |=
        TC_CTRLA_WAVEGEN_MFRQ |      // select match frequency mode
        TC_CTRLA_PRESCALER_DIV256 | // devide the clock by 256
        TC_CTRLA_ENABLE;             // start the timer
    while (TC3->COUNT16.STATUS.bit.SYNCBUSY);

    /* set the compare register
     *  The counter will count up to this value, generate an interrupt,
     *  and then start over from zero. The time between intertupts is
     *  t = compare_val * prescaler / ((8 * 10^6) / GEN_DIV)
     */

	// BE CAREFUL - this can mess with button presses if set much higher.
    uint16_t compare_val = 33; // I last set this to be about 1ms
    TC3->COUNT16.CC[0].reg = compare_val;
    while (TC3->COUNT16.STATUS.bit.SYNCBUSY);

    // Configure interrupt request
    NVIC_DisableIRQ(TC3_IRQn);
    NVIC_ClearPendingIRQ(TC3_IRQn);
    NVIC_SetPriority(TC3_IRQn, 0);
    NVIC_EnableIRQ(TC3_IRQn);

    // Enable the TC3 interrupt request
    TC3->COUNT16.INTENSET.bit.MC0 = 1;
    while (TC3->COUNT16.STATUS.bit.SYNCBUSY);
}

void init_encoder()
{
  // CLOCK SETUP
  // Enable and configure generic clock generator 5
  // Note that generators 0-3 are used by the IDE
  GCLK->GENCTRL.reg = GCLK_GENCTRL_IDC |            // Improve duty cycle
                      GCLK_GENCTRL_GENEN |          // Enable generic clock gen
                      GCLK_GENCTRL_SRC_DFLL48M |    // Select 48MHz as source
                      GCLK_GENCTRL_ID(5);           // Select GCLK5
  while (GCLK->STATUS.bit.SYNCBUSY);      // Wait for synchronization

  // Set clock divider of 1 to generic clock generator 5
  GCLK->GENDIV.reg = GCLK_GENDIV_DIV(1) |           // Divide 48 MHz by 1
                     GCLK_GENDIV_ID(5);             // Apply to GCLK4 5
  while (GCLK->STATUS.bit.SYNCBUSY);      // Wait for synchronization


  // Enable GCLK4 and connect it to EIC
  GCLK->CLKCTRL.reg = GCLK_CLKCTRL_CLKEN |    // Enable generic clock
                      GCLK_CLKCTRL_GEN_GCLK5 |       // Select GCLK5
                      0x0005;                      // Feed GCLK5 to EIC
  while (GCLK->STATUS.bit.SYNCBUSY);          // Wait for synchronization

  EIC->CTRL.bit.SWRST = 1;
  while (EIC->STATUS.bit.SYNCBUSY);          // Wait for synchronization

  // Set input pins
  PORT->Group[0].DIR.bit.DIR |= 1 << 5;     // I too, like to name child
  PORT->Group[0].DIR.bit.DIR |= 1 << 7;     // variables the same as the
  PORT->Group[1].DIR.bit.DIR |= 1 << 9;
  //                                        // parent.

  // Setup pins with config register in PORT
  PORT->Group[0].WRCONFIG.reg = 0x50470020;
  PORT->Group[0].WRCONFIG.reg = 0x50470080;
  PORT->Group[1].WRCONFIG.reg = 0x50470200;  // PB9, encoder button
  	//0bFEDC BA98 7654 3210
  while (EIC->STATUS.bit.SYNCBUSY);          // Wait for synchronization

  // Setup interrupt pins in EIC
  EIC->INTENSET.bit.EXTINT5 = 1;
  EIC->INTENSET.bit.EXTINT7 = 1;
  EIC->INTENSET.bit.EXTINT9 = 1;
  while (EIC->STATUS.bit.SYNCBUSY);          // Wait for synchronization

  // Setup EIC config
  EIC->CONFIG[0].bit.FILTEN7 = 1; // Enable filters for better reading
  EIC->CONFIG[0].bit.SENSE7 = 0b011; // BOTH edge
  EIC->CONFIG[0].bit.FILTEN5 = 1;
  EIC->CONFIG[0].bit.SENSE5 = 0b011; // BOTH edge
  EIC->CONFIG[1].bit.FILTEN1 = 1;
  EIC->CONFIG[1].bit.SENSE1 = 0b011; // BOTH edge
  while (EIC->STATUS.bit.SYNCBUSY);          // Wait for synchronization

  // Enable CLK_EIC                       - X
  // PINCFG4, PMUXEN = 1                  - X
  // PMUX3.PMUXE = 0x00                   - X
  // EIC.CTRL.ENABLE = 1                  - X
  // CONFIG0.FILTEN = 0                   - X
  // CONFIG0.SENSE4 = 0b001 for RISING    - X
  // INTENSET.EXTINTx to 1                - X

  EIC->CTRL.bit.ENABLE = 1;
  while (EIC->STATUS.bit.SYNCBUSY);          // Wait for synchronization

  // Set pins to PULL-UP
  PORT->Group[0].OUT.reg |= (1 << 5);
  PORT->Group[0].OUT.reg |= (1 << 7);
  PORT->Group[1].OUT.reg |= (1 << 9);


  // Configure interrupt request
  // See https://static.docs.arm.com/ddi0419/d/DDI0419D_armv6m_arm.pdf
  NVIC_DisableIRQ(EIC_IRQn);
  NVIC_ClearPendingIRQ(EIC_IRQn);
  NVIC_SetPriority(EIC_IRQn, 0);
  NVIC_EnableIRQ(EIC_IRQn);
}



// Fault handler
// ARMv6-M doesn't support telling you what kind of fault occured.
void HardFault_Handler(void)
{
	// Print error
	SerialUSB.println("\tHARD FAULT!");

	// Reset
	delay(1000);
	NVIC_SystemReset();

}

#ifdef __arm__
// should use uinstd.h to define sbrk but Due causes a conflict
extern "C" char* sbrk(int incr);
#else  // __ARM__
extern char *__brkval;
#endif  // __arm__

int freeMemory() {
  char top;
#ifdef __arm__
  return &top - reinterpret_cast<char*>(sbrk(0));
#elif defined(CORE_TEENSY) || (ARDUINO > 103 && ARDUINO != 151)
  return &top - __brkval;
#else  // __arm__
  return __brkval ? &top - __brkval : &top - __malloc_heap_start;
#endif  // __arm__
}



// Throwing sharp objects into a dark room
void addrScan() {
  byte error, address;
  int nDevices;
  SerialUSB.println("Scanning...");
  nDevices = 0;

  for (address = 1; address < 127; address++) {
    //I2C.initReadByte(address);
    //I2C.read();
    I2C.writeByte(address, 0, 0);
    I2C.readByte(address);
    while (I2C.readBusy);
    error = I2C.getData();
    if (error != 0) {
      SerialUSB.print("Scanning at: ");
      SerialUSB.println(address, HEX);
      SerialUSB.print("Error: ");
      SerialUSB.println(error);
      error = 0;
      nDevices++;
    }

  }
  if (nDevices == 0)
    SerialUSB.println("No I2C devices found\n");
  else
    SerialUSB.println("done\n");
  delay(5000); // wait 5 seconds for next scan
}
