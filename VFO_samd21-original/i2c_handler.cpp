#include "i2c_handler.h"


void startI2C()
{
  // Yes, these chips DO appear to use SERCOM_ALT... I'm not entirely sure what that means.
  I2C.begin(400000, REG_ADDR_8BIT, PIO_SERCOM_ALT);            // Start at 400kHz
}


void I2C_writeBytes(uint8_t devAddress, uint16_t regAddress, uint8_t* data, uint8_t count)
{
  I2C.writeBytes(devAddress, regAddress, data, count);
}

void I2C_writeByte(uint8_t devAddress, uint16_t regAddress, uint8_t data)
{
  I2C.writeByte(devAddress, regAddress, data);
}
