#include <I2C_DMAC.h>

/*
 * I'm pretty sure these two files (i2c_handler.h and i2c_handler.cpp) are completely useless. 
 */

// I2C Functions
void startI2C();
void I2C_writeBytes(uint8_t devAddress, uint16_t regAddress, uint8_t* data, uint8_t count);
void I2C_writeByte(uint8_t devAddress, uint16_t regAddress, uint8_t data);
