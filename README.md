This is a Variable Frequency Oscillator (VFO) project based on the SAMD21 ARM microcontroller. The module operates with a Si5351 clock board from adafruit, and a SSD1306 OLED display. An encoder is used to control the UI.

_Project Objectives_
- Triple outputs up to 100Mhz
- Single output up to 200Mhz
- Quadrature synchronized outputs
- Configurable phase offsets
- Configurable individual clocks
- Programmable memory recall
- Variable power output
- Modular and extensible PCB for adding companion boards later.

