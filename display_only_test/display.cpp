#include "fonts.h"
#include "display.h"
//#include "i2c_handler.h"
#include <I2C_DMAC.h>
#include <stdbool.h>


#define DISP_ADDR 0x3C

#define SCREEN_HEIGHT 64
#define SCREEN_WIDTH 128

static uint8_t dispBuffer[SCREEN_WIDTH * SCREEN_HEIGHT / 8];

// Screen object
static SSD1306_t SSD1306;

bool initSSD1306() {

  // Init OLED
  sendDispCommand(SSD1306_DISPLAYOFF); //display off

  sendDispCommand(SSD1306_MEMORYMODE); //Set Memory Addressing Mode
  sendDispCommand(0x00); // 00b,Horizontal Addressing Mode; 01b,Vertical Addressing Mode;
  // 10b,Page Addressing Mode (RESET); 11b,Invalid

  sendDispCommand(0xB0); //Set Page Start Address for Page Addressing Mode,0-7

  sendDispCommand(SSD1306_COMSCANDEC); //Set COM Output Scan Direction

  /*
  sendDispCommand(SSD1306_SETLOWCOLUMN); //---set low column address
  sendDispCommand(SSD1306_SETHIGHCOLUMN); //---set high column address
  */

  sendDispCommand(SSD1306_SETSTARTLINE); //--set start line address - CHECK
  sendDispCommand(0x00);

  sendDispCommand(SSD1306_SETCONTRAST); //--set contrast control register - CHECK
  sendDispCommand(0xFF);

  // This mirrors the display if not set?
  sendDispCommand(0xA1); //--set segment re-map 0 to 127 - CHECK

  sendDispCommand(SSD1306_NORMALDISPLAY); //--set normal color

  sendDispCommand(SSD1306_SETMULTIPLEX); //--set multiplex ratio(1 to 64) - CHECK
  sendDispCommand(0x3F); // height - 1

  sendDispCommand(0xA4); //0xa4,Output follows RAM content;0xa5,Output ignores RAM content

  sendDispCommand(SSD1306_SETDISPLAYOFFSET); //-set display offset - CHECK
  sendDispCommand(0x00); //-not offset

  sendDispCommand(SSD1306_SETDISPLAYCLOCKDIV); //--set display clock divide ratio/oscillator frequency
  sendDispCommand(0xF0); //--set divide ratio

  sendDispCommand(SSD1306_SETPRECHARGE); //--set pre-charge period
  sendDispCommand(0x22); //

  sendDispCommand(SSD1306_SETCOMPINS); //--set com pins hardware configuration - CHECK
  sendDispCommand(0x12); // 0x02 for 128x32, 0x12 for 126x64

  sendDispCommand(SSD1306_SETVCOMDETECT); //--set vcomh
  sendDispCommand(0x20); //0x20,0.77xVcc

  sendDispCommand(SSD1306_CHARGEPUMP); //--set DC-DC enable
  sendDispCommand(0x14); //
  sendDispCommand(SSD1306_DISPLAYON); //--turn on SSD1306 panel

  // Set default values for screen object
  SSD1306.CurrentX = 0;
  SSD1306.CurrentY = 0;

  SSD1306.Initialized = 1;

  return true;
}

void pushDisplay() {

  for (uint8_t i = 0; i < SCREEN_HEIGHT / 8; i++) {
    sendDispCommand(0xB0 + i); // Set the current RAM page address.
    sendDispCommand(0x00);
    sendDispCommand(0x10);
    I2C.writeBytes(DISP_ADDR, 0x40, &dispBuffer[SCREEN_WIDTH * i], SCREEN_WIDTH);
    //ssd1306_WriteData(&SSD1306_Buffer[SSD1306_WIDTH*i],SSD1306_WIDTH);
  }
}



void displayDrawPixel(uint8_t x, uint8_t y, bool on) {

  uint16_t index = x + (y / 8) * SCREEN_WIDTH;
  if (index >= sizeof(dispBuffer)) {
  	// Make sure index is within bounds
    	return;
  }
  if (on)
  {
    dispBuffer[index ] |= 1 << (y % 8);
  } else {
    dispBuffer[x + (y / 8) * SCREEN_WIDTH] &= ~(1 << (y % 8));
  }

}

void displayFillArea(uint8_t x1, uint8_t y1, uint8_t x2, uint8_t y2, bool on)
{
  uint8_t delta_x = x2-x1;
  uint8_t delta_y = y2-y1;

  uint8_t progY;// rename?
  uint8_t progX;

  // Rows -> Columns
  for (progY = 0; progY < delta_y; progY++)
  {
    for (progX = 0; progX < delta_x; progX++)
    {
      displayDrawPixel(x1 + progX, y1 + progY, on);
    }
  }
}

void wipeDisplay() {
  for (uint32_t i = 0; i < sizeof(dispBuffer); i++) {
    dispBuffer[i] = 0x00;
  }
}

void fillDisplay() {
  for (uint32_t i = 0; i < sizeof(dispBuffer); i++) {
    dispBuffer[i] = 0xFF;
  }
}
void sendDispCommand(uint8_t command)
{
  I2C.writeByte(DISP_ADDR, 0x00, command);
  //I2C_writeByte(DISP_ADDR, 0x00, command);
}

void sendDispCommands(uint8_t* command, uint8_t count)
{
  I2C.writeBytes(DISP_ADDR, 0x00, command, count);
  //I2C_writeBytes(DISP_ADDR, 0x00, command, count);
}


char displayDrawCharacter(char ch, FontDef Font, bool pixelState)
{
  uint32_t i, b, j;

  // Check if character is valid
  if (ch < 32 || ch > 126)
    return 0;

  // Check remaining space on current line
  if (SCREEN_WIDTH < (SSD1306.CurrentX + Font.FontWidth) ||
      SCREEN_HEIGHT < (SSD1306.CurrentY + Font.FontHeight))
  {
    // Not enough space on current line
    return 0;
  }

  // Use the font to write
  for (i = 0; i < Font.FontHeight; i++) {
    b = Font.data[(ch - 32) * Font.FontHeight + i];
    for (j = 0; j < Font.FontWidth; j++) {
      if ((b << j) & 0x8000)  {
        displayDrawPixel(SSD1306.CurrentX + j, (SSD1306.CurrentY + i), true);
      } else {
        displayDrawPixel(SSD1306.CurrentX + j, (SSD1306.CurrentY + i), false);
      }
    }
  }

  // The current space is now taken
  SSD1306.CurrentX += Font.FontWidth;

  // Return written char for validation
  return ch;
}

char displayDrawString(char* str, FontDef Font, bool pixelState)
{
  // Write until null-byte
  while (*str) {
    if (displayDrawCharacter(*str, Font, pixelState) != *str) {
      // Char could not be written
      return *str;
    }

    // Next char
    str++;
  }

  // Everything ok
  return *str;
}

void displaySetCursor(uint8_t x, uint8_t y)
{
  SSD1306.CurrentX = x;
  SSD1306.CurrentY = y;
}

void testDisplay() {
  // This will remain like this to show the brute-force method.
  sendDispCommand(SSD1306_DISPLAYOFF);
  sendDispCommand(SSD1306_SETDISPLAYCLOCKDIV);
  sendDispCommand(0x80);
  sendDispCommand(SSD1306_SETMULTIPLEX);
  sendDispCommand(SCREEN_HEIGHT - 1);
  sendDispCommand(SSD1306_SETDISPLAYOFFSET);
  sendDispCommand(0x00);
  sendDispCommand(SSD1306_SETSTARTLINE | 0x00);
  sendDispCommand(SSD1306_CHARGEPUMP);
  sendDispCommand(0x14);
  sendDispCommand(SSD1306_DISPLAYALLON_RESUME);
  sendDispCommand(SSD1306_SETCOMPINS);
  sendDispCommand(0x12);
  sendDispCommand(SSD1306_SETCONTRAST);
  sendDispCommand(255);
  sendDispCommand(SSD1306_SETPRECHARGE);
  sendDispCommand(0xF1);
  sendDispCommand(SSD1306_DISPLAYON);
}
