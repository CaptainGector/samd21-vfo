

// Essentially:
/* 
 * Panes with tabs
 * 
 *
 *
 *
 */


/* -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -*
 *                                         |           |           *
 *                                         |   VFO     | Settings  *
 *                                         ------------------------*
 *            __    __   __   __     __   __   __                  *
 *              |  |  | |  | |  |   |  | |  | |  |   |  | __       *
 *   CLK0      /   |  | |  | |  |   |  | |  | |  |   |--|  /       *
 *            |  . |__| |__| |__| . |__| |__| |__|   |  | /_       *
 *                                                                 *
 *                      CLK1 - 10.100.000 Hz                       *
 *                      CLK2 -  6.102.500 Hz                       *
 * -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -*
 */


// You need to be able to set every clock easily. A long press will switch between clocks.

// Display aside, there needs to at least be a settings system.
/*
 * Settings:
 * 	----------------------------------------
 *	* Power Output            |    8mA     |
 * 	----------------------------------------
 *	* Phase offset            |    90deg   |
 * 	----------------------------------------
 *	* Phase enable            |    OFF     |
 * 	----------------------------------------
 *			........ etc.
 * 	----------------------------------------
 *
 *  Essentially, settings have a single-value adjustment.
 *  Potential Structure:
 *  	power_output is of type power enum
 *  	phase_offset is of type integer
 *  	phase_enable is of type integer or boolean
 *
 *  Code responsible for displaying settings must have a set format
 *  	Row Height
 *	
 *  For the VFO display, there needs to be a special adapter from the internal
 *  frequency value to the text shown to the user.
 *  		7.100.000
 *  The first two digits should automatically expand to double and triple 
 *  digits, but never drop past 0.
 *
 *  There should be two modes of entry for the VFO - precisie and fluid.
 *  	Fluid modification will have acceleration and increment the value
 *  	in Hz or Khz
 *  
 *  	Precise modification requires the use of the encoder button presses, 
 *  	which means that other events cannot use that.
 *
 *
 *  Event system:
 *  	For the sake of easily modifying the UI and it's features, it is
 *  	advantagous to use an event system.
 *
 *  	This will be done with a FIFO buffer. Interrupts from the encoder
 *  	and of the buttons will be responsible for adding events to the
 *  	buffer. Types of events may include:
 *  		* Encoder turn left
 *  		* Encoder turn right
 *  		* Encoder Press 
 *  		* Encoder Held
 *  	There is a problem of event detection when all of them involve
 *  	the same action of pressing a button. Detecting a held encoder
 *  	will be done via edge detection, the "press" event will be fired
 *  	on a short duration and the held event will be fired on a long
 *  	duration. Interrupts will be set to start/stop the time measurement
 *  	on rising and falling edges.
 *  	
 *  	Double Taps:
 *  		For the sake of implementing more features like touble-click
 *  		support, the FIFO buffer will have an associated data
 *  		structure with it. The data structurure will contain
 *  		information like the time since the last press
 *
 *  	First-in-First-Out Buffer:
 *  		At the core, this can probably be something like an array
 *  		of enums. A function will probably have to be executed to
 *  		keep this structure correctly set up. I should RESEARCH this.
 *	
 *	
 *  As soon as a working event system is in place, it will be possible to
 *  tweak the user input experience until it feels right. Theoretically,
 *  this will be the schema for input:
 *  	* Double tap will open/close settings
 *  	* Rotating the encoder will select next setting,
 *  	or modify any activated selections.
 * 	* Clicking in settings will edit a setting,
 * 	clicking again will save that setting. 
 *
 *
 *
 * ALL SETTINGS
 * 	* Power output
 * 	* Phase offset
 * 	* Phase enable
 * 	* Step value
 * 	* Entry mode
 * 	* Clk 0 enable
 * 	* Clk 1 enable
 * 	* Clk 2 enable
 *
 * EVENTs!
 * 	Okay so. This is hard.
 * 	We need to know when the button is pressed the first time
 *	And we nned to know when the button is released.
 *	We can't do this with simply RISING/FALLING edge detection
 *	beceause that can be either a RELEASE/PRESS or PRESS/RELEASE.
 *
 *	Using the Edge Detection:
 *		Upon a edge, read the value of the pin and compare it to the last.
 *		If the previous level was HIGH, then we got a PRESS. If the event
 *		is short, we fire the PRESS event. If the event was long, we 
 *		fire the HELD event.
 *		In determining a double-press event, the first PRESS event will be 
 *		sent regardless. It can be revoked however, if not already read by
 *		the UI. So, to get a third input option, we can use two together.
 *		This is the same as the binary idea, 00->01->10 and so on. For
 *		user convienence, we combine HELD and TURN events to change pages
 *		and escape the 2D problem.
 *		
 *		
 *
 *
 *
 */
