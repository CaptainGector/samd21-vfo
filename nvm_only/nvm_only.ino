// Written by gector whitlox
// Purpose is to test and demonstrate the use of the non-volatile memory


void setup() {
	SerialUSB.begin(115200); 
	while(!SerialUSB);
	SerialUSB.println("Hallo world");

	// Check errors?
	uint32_t status;
	status = NVMCTRL->STATUS.reg;
	SerialUSB.print("NVM Status: ");
	SerialUSB.println(status, BIN);

	uint32_t dataReciever;

	// Data to write
	uint32_t data[16] = {};
	data[0] = 0xF0F0F0F0;
	
	// pointer to data
	uint32_t *dataSrc = data; // Bearing in mind that array is pointer to start.

	// User data (I don't know where this ends, this might be 1 row?)
	//uint32_t userDataRow0Page0    = 0x804000; //0x00804000;
	// NVM test location, row 1000 out of 1024
	uint32_t dst = 0x3E800;
	// I have no idea what it's getting on about userDataRow?? 	0x00804000 vs
	//								0x0003E800 ??
	// Final address is 0x40000??
	// WHY DID THEY TRICK ME
	// (late date) I think it might still be possible to write there, but I'm not
	// gonna even try, since the RWW (read-while-write) area is for EEPROM stuff.
	// If necessary I'll put stuff in the user row but not if I can help it.

	uint32_t dstRWW = 0x00400000; // Read-while-write area, for EEPROM.
	
	dst = dstRWW;

	//uint32_t *destination = (uint32_t *)userDataRow0Page0;

	// Try retrieving some of that yummy data
	dataReciever = *(uint32_t *)dst;
	SerialUSB.print("Data: ");
	SerialUSB.print(dataReciever, HEX);
	SerialUSB.print(", ");
	SerialUSB.println(dataReciever);

	// NVM is comprised of 4 pages per row
	// Pages are 64Bytes big
	// print out page size just to make sure
	uint32_t pageSize = NVMCTRL->PARAM.bit.PSZ;
	SerialUSB.print("Page size: ");
	SerialUSB.println(pageSize, HEX);

	
	if (dataReciever != data[0]) {
		SerialUSB.println("Writing to NVM...");

		// -- Write to NVM -- Register at 0x41004400?
		while(!(NVMCTRL->INTFLAG.bit.READY));

		// Write sequence for manual:
		// Write to page buffer simply by addressing the memory location
		// Write the page buffer to memory by using the CTRL.CMD='Write Page' and CMDEX
		// Wait for ready bit :)

		// Enable manual write mode
		NVMCTRL->CTRLB.bit.MANW = 1;
		while(!(NVMCTRL->INTFLAG.bit.READY));

		// Write address so we can erase first 
		NVMCTRL->ADDR.reg = dst; // Apperently 16bit means 32bit/2???
		while(!(NVMCTRL->INTFLAG.bit.READY));

		// Erase data there
		NVMCTRL->CTRLA.reg = NVMCTRL_CTRLA_CMDEX_KEY | NVMCTRL_CTRLA_CMD_ER; 
		while(!(NVMCTRL->INTFLAG.bit.READY));

		// clear the page buffer
		NVMCTRL->CTRLA.reg = NVMCTRL_CTRLA_CMDEX_KEY | NVMCTRL_CTRLA_CMD_PBC;
		while (NVMCTRL->INTFLAG.bit.READY == 0);

		// fill page buffer
		//uint32_t len = 15;//pageSize>>2;
		//while (len--)
		//	*destination++ = *dataSrc++;
		

		*(uint32_t*)dst = data[0]; // 32 bit writes
		//*(uint32_t*)userDataRow0Page1 = 0x00000000; // 32 bit writes
		//*(uint32_t*)userDataRow0Page2 = 0x00000000; // 32 bit writes
		//*(uint32_t*)userDataRow0Page3 = 0x00000000; // 32 bit writes

		NVMCTRL->CTRLA.reg = NVMCTRL_CTRLA_CMDEX_KEY | NVMCTRL_CTRLA_CMD_WP; // "Write Page"
		while(!(NVMCTRL->INTFLAG.bit.READY));
	}
	// -- Read from NVM --

	// Try retrieving some of that yummy data
	dataReciever = *(uint32_t *)dst;
	SerialUSB.print("Data: ");
	SerialUSB.print(dataReciever, HEX);
	SerialUSB.print(", ");
	SerialUSB.println(dataReciever);

	// Check status aftwards
	status = NVMCTRL->STATUS.reg;
	SerialUSB.print("NVM Status: ");
	SerialUSB.println(status, BIN);

}

void loop() {
	// put your main code here, to run repeatedly:

}
