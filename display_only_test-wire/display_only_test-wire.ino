#include "display.h"
//#include <Wire.h>
//#include <I2C_DMAC.h>

  
void setup() {
  SerialUSB.begin(57600);
  while (!SerialUSB);                 // Uncomment if MUST wait for serial bus (serial monitor MUST be open)
  delay(100);

  pinMode(7, INPUT_PULLUP);
  
  //I2C.begin(400000, REG_ADDR_8BIT, PIO_SERCOM_ALT);            // Start at 400kHz
  //I2C.begin(400000, REG_ADDR_8BIT, PIO_SERCOM);
  //myWire.begin();
  
  SerialUSB.println("Initilized.");

  initSSD1306(); // Should show some noise on the display.

  SerialUSB.println("Malloc success.");

  delay(1000);
  SerialUSB.println("Pushing display full");
  fillDisplay();
  pushDisplay();
  delay(500);
  wipeDisplay();
  pushDisplay();

  delay(300);
  // Write a string to display.
  char error = displayDrawString((char*)"J is K!", Font_16x26, true);
  pushDisplay();

  delay(750);
  wipeDisplay();

	int startY = 14;
	int subYDist = 21;
	
	displaySetCursor(4, startY);
  displayDrawString((char*)"7.747.000", Font_11x18, true);
	displaySetCursor(104, startY);
  displayDrawString((char*)"Clk0", Font_6x8, true);
	displaySetCursor(104, startY+9);
  displayDrawString((char*)"Hz", Font_7x10, true);
	displaySetCursor(0, subYDist + startY);
  displayDrawString((char*)"Clk1", Font_6x8, true);
	displaySetCursor(30, subYDist + startY);
  displayDrawString((char*)"10.100.000", Font_6x8, true);
	displaySetCursor(12*8+2, subYDist + startY);
  displayDrawString((char*)"Hz", Font_6x8, true);
	displaySetCursor(0, subYDist + startY + 10);
  displayDrawString((char*)"Clk2", Font_6x8, true);
	displaySetCursor(30, subYDist + startY + 10);
  displayDrawString((char*)"30.500.000", Font_6x8, true);
	displaySetCursor(12*8+2, subYDist + startY + 10);
  displayDrawString((char*)"Hz", Font_6x8, true);
  pushDisplay();
	delay(1500);
	// Flash the display
	for (int x = 0; x < 2; x++) {
		fillDisplay();
		pushDisplay();
		delay(25);
		wipeDisplay();
		pushDisplay();
		delay(25);
	}

	wipeDisplay();
	int HlineSpacing = 16;
	int xMargin = 4;
	displaySetCursor(xMargin, HlineSpacing-11);
  	displayDrawString((char*)"Phase E   | ON", Font_7x10, true);

	displaySetCursor(xMargin, HlineSpacing*2-11);
  	displayDrawString((char*)"Phase S   | 90", Font_7x10, true);

	displaySetCursor(xMargin, HlineSpacing*3-11);
  	displayDrawString((char*)"Step Value| 1.5k", Font_7x10, true);

	displaySetCursor(xMargin, HlineSpacing*4-11);
  	displayDrawString((char*)"Entry Mode| SET", Font_7x10, true);

	displayFillArea(0, HlineSpacing*1, 127, HlineSpacing*1+1, true);
	displayFillArea(0, HlineSpacing*2, 127, HlineSpacing*2+1, true);
	displayFillArea(0, HlineSpacing*3, 127, HlineSpacing*3+1, true);
	displayFillArea(126, 0, 127, 64, true);
	displayFillArea(0, 0, 1, 64, true);
  	pushDisplay();
}

void loop() {
  if (digitalRead(7))
  {
    displayFillArea(120, 11, 125, 16, true);
  } else
  {
    displayFillArea(120, 11, 125, 16, false);
  }

  pushDisplay();
  delay(500);
}
