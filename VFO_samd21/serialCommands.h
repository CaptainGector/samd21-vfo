/*
So. We need a set of serial commands to control aspects of the VFO operation
- How do show up as serial device?

Commands (or command sets):
- Set frequency for clock 'x'       # SET_FREQ 0 13000                          - Set frequency of CLK0 to 13,000Hz
- Set power level of clock 'x'      # SET_POWER 0 3                             - Set power level of CLK0 to level 3
- Turn clock 'x' on/off             # SET_ENABLED 0 0                           - Disable CLK0
- Sweep clock 'x' over n ms         # SWEEP 0 1000 2000 20                      - Sweep CLK0 from 1000Hz to 2000Hz in 20ms
- Set phase of clock 'x'            # SET_PHASE 0 55                            - Set phase of CLK0 to 55 degrees



*/
