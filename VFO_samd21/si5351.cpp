/*  Title: SAMD21_VFO Si5351 Control Program, Author: Zachary Whitlock

    Code is largely taken from Bitx40/Raduino https://github.com/amunters/bitx40/blob/master/raduino_v1.29.ino#L13,
    July 10th, 2020.
*/

#include "si5351.h"


#define SI5351_ADDR 0x60

#define SI5351BX_XTAL 25000000          // Crystal freq in Hz
#define SI5351BX_MSA  35                // VCOA is at 25mhz*35 = 875mhz

#define BB0(x) ((uint8_t)x)             // Bust int32 into Bytes
#define BB1(x) ((uint8_t)(x>>8))
#define BB2(x) ((uint8_t)(x>>16))

// User program may have reason to poke new values into these 3 RAM variables
uint32_t si5351bx_vcoa = (SI5351BX_XTAL*SI5351BX_MSA);  // 25mhzXtal calibrate
uint8_t  si5351bx_rdiv = 0;             // 0-7, CLK pin sees fout/(2**rdiv)
// 0=2ma 1=4ma 2=6ma 3=8ma for CLK 0,1,2
extern uint8_t  si5351bx_drive[3];
uint8_t  si5351bx_clken = 0xFF;         // Private, all CLK output drivers off

void si5351_init() {                  // Call once at power-up, start PLLA
  Wire.begin();
  uint8_t reg;  uint32_t msxp1;

  si5351_i2c_WriteByte(SI5351_ADDR, 149, 0); // Disable Spread Spectrum
  si5351_i2c_WriteByte(SI5351_ADDR, 3, 0xFF); // Disable all clocks
  si5351_i2c_WriteByte(SI5351_ADDR, 183, ((2 << 6) | 0x12)); // Set 25mhz crystal load
  msxp1 = 128 * 35 - 512;
  uint8_t  vals[8] = {0, 1, BB2(msxp1), BB1(msxp1), BB0(msxp1), 0, 0, 0};
  si5351_i2c_WriteBytes(SI5351_ADDR, 26, vals, 8); // Multisynth NA stuff
  si5351_i2c_WriteByte(SI5351_ADDR, 177, 0x20); // Reset PLL

  // Clear phase offsets
  si5351_i2c_WriteByte(SI5351_ADDR, 165, 0x00);
  si5351_i2c_WriteByte(SI5351_ADDR, 166, 0x00);
  si5351_i2c_WriteByte(SI5351_ADDR, 167, 0x00);
}


void si5351_setfreq(uint8_t clknum, uint32_t fout, bool invert) {  // Set a CLK to fout Hz
  uint32_t  msa, msb, msc, msxp1, msxp2, msxp3p2top;
  if ((fout < 500000) || (fout > 109000000)) // If clock freq out of range
    si5351bx_clken |= 1 << clknum;      //  shut down the clock
  else {
    msa = si5351bx_vcoa / fout;     // Integer part of vco/fout
    msb = si5351bx_vcoa % fout;     // Fractional part of vco/fout
    msc = fout;             // Divide by 2 till fits in reg
    while (msc & 0xfff00000) {
      msb = msb >> 1;
      msc = msc >> 1;
    }
    msxp1 = (128 * msa + 128 * msb / msc - 512) | (((uint32_t)si5351bx_rdiv) << 20);
    msxp2 = 128 * msb - 128 * msb / msc * msc; // msxp3 == msc;
    msxp3p2top = (((msc & 0x0F0000) << 4) | msxp2);     // 2 top nibbles
    uint8_t vals[8] = { BB1(msc), BB0(msc), BB2(msxp1), BB1(msxp1),
                        BB0(msxp1), BB2(msxp3p2top), BB1(msxp2), BB0(msxp2)
                      };
    si5351_i2c_WriteBytes(SI5351_ADDR, 42 + (clknum * 8), vals, 8); // Write to 8 msynth regs
    if (invert) {
  	  si5351_i2c_WriteByte(SI5351_ADDR, 16 + clknum, 0x1C | si5351bx_drive[clknum]); // use local msynth
    } else {
    	si5351_i2c_WriteByte(SI5351_ADDR, 16 + clknum, 0x0C | si5351bx_drive[clknum]); // use local msynth
    }
    //}
    si5351bx_clken &= ~(1 << clknum);   // Clear bit to enable clock
  }

  // Inform CLK 2 that it will have a phase offset appropriate to CLK 1 and 90 degrees
  /*static uint32_t t_offset;
  if (clknum = 1)
     t_offset = (1/4) * (1/fout) * 4 * 875000000;
  // If t_offset exceeds 7 bits, reverse clock and recalculate
  if (t_offset > 127) {
     t_offset = (3/4) * (1/fout) * 4 * 875000000;
     I2C.writeByte(SI5351_ADDR, 167, t_offset);
  }
  I2C.writeByte(SI5351_ADDR, 167, t_offset);*/

  //I2C.writeByte(SI5351_ADDR, 3, si5351bx_clken);        // Enable/disable clock
  si5351_i2c_WriteByte(SI5351_ADDR, 3, si5351bx_clken);        // Enable/disable clock
}

void si5351_i2c_WriteByte(uint8_t devAddr, uint8_t addr, uint8_t data) {
  Wire.beginTransmission(devAddr);
  Wire.write(addr);
  Wire.write(data);
  Wire.endTransmission();
}

void si5351_i2c_WriteBytes(uint8_t devAddr, uint8_t addr, uint8_t* data, uint8_t len) {
  Wire.beginTransmission(devAddr);
  Wire.write(addr);
  for (int c = 0; c < len; c++) {
    //Wire.write(dispBuffer[SCREEN_WIDTH * i + c]);
    Wire.write(data[c]);
  }
  Wire.endTransmission();
}
