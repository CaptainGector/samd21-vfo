//#include <I2C_DMAC.h>
#include "Wire.h"

// Function prototypes
void si5351_init();
void si5351_setfreq(uint8_t clknum, uint32_t fout, bool invert);

void si5351_i2c_WriteByte(uint8_t devAddr, uint8_t addr, uint8_t data);
void si5351_i2c_WriteBytes(uint8_t devAddr, uint8_t addr, uint8_t *data, uint8_t len);
