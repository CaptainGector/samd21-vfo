/*
   eepromSim.cpp by Zachary Whitlock

   Written for the samd21 VFO, a ham radio project.

	This file contains the code for reading/writing
	to the Non-Volatile Memory (NVM).
*/

#include "eepromSim.h"


uint32_t pageBuffer[EEPROM_PAGE_SIZE/4];
uint32_t rowBuffer[EEPROM_ROW_SIZE/4];

// 	Init
// Tell the NVM controller how much EEPROM we're using at the end
// 	Read/write user row to set EEPROM size
// Instantiate stuff
void eepromInit() {
	// Set up the userRow (TODO)

	// Zero the buffers
	uint32_t i;
	for (i = 0; i < EEPROM_PAGE_SIZE/4; i++){
		pageBuffer[i] = 0;
	}
	for (i = 0; i < EEPROM_ROW_SIZE/4; i++){
		rowBuffer[i] = 0;
	}

	// Set write mode
	NVMCTRL->CTRLB.bit.MANW = 1; // manual
	while(!(NVMCTRL->INTFLAG.bit.READY));
}

//	Copy & Write a Page
// Preserves the data that was in NVM already, and only overwrites
// the the page we're referring to
void eepromSafeWritePage(uint32_t row_num, uint32_t page_num, uint32_t *dataSrc) {
	// Wait for ready.
	while(!(NVMCTRL->INTFLAG.bit.READY));

	// Page offset calculation
	uint16_t pageOffset;
	pageOffset = page_num * (EEPROM_PAGE_SIZE/4);

	// 	Copy entire row to buffer
	eepromReadRow(row_num);
	while(!(NVMCTRL->INTFLAG.bit.READY));

	// 	Erase row in memory
	eepromEraseRow(row_num);
	while(!(NVMCTRL->INTFLAG.bit.READY));

	// 	Modify buffer
	// Copy data from the data-source into the row buffer
	uint8_t r;
	for (r = 0; r < EEPROM_PAGE_SIZE/4; r++) { // normally runs 16 times
		rowBuffer[pageOffset+r] = dataSrc[r];
	}

	// 	Re-write row
	for (r = 0; r < 4; r++) {
		// Wait for ready.
		while(!(NVMCTRL->INTFLAG.bit.READY));
		eepromWritePage(row_num, r, &rowBuffer[16*r]);
	}
}

// 	Read Page
// Reads an entire page from NVM into pageBuffer
void eepromReadPage(uint32_t row_num, uint32_t page_num){
	// Iterate over the given page and save the contents to the page buffer
	// Note: Higher addresses are saved at a higher index
	uint32_t startingAddress;
	//  PageNum = (RowNum*4)+PagePosInRow
	//  PageAddr = (PageNum) * PageSize
	startingAddress = ((row_num*4) + page_num) * EEPROM_PAGE_SIZE;
	uint32_t len;
	for (len = 0; len < EEPROM_PAGE_SIZE/4; len++) {
		while(!(NVMCTRL->INTFLAG.bit.READY));
		pageBuffer[len] = *(uint32_t*)startingAddress;
		startingAddress += 4;
	}
}

//	Read Row
// Reads an entire row from NVM into rowBuffer;
void eepromReadRow(uint32_t row_num) {

	// Make sure we're actually ready to read
	while(!(NVMCTRL->INTFLAG.bit.READY));

	// Read a page and then move the data into rowBuffer
	uint32_t page;
	for (page = 0; page < 4; page++) {
		eepromReadPage(row_num, page);
		// Copy into page buffer
		memcpy(rowBuffer+(NVMCTRL_PAGE_SIZE/4)*page, pageBuffer, NVMCTRL_PAGE_SIZE);
	}
}

// 	Write Page
// Writes a page to NVM
// Erase row FIRST!
uint32_t eepromWritePage(uint32_t row_num, uint32_t page_num, uint32_t *dataSrc) {
	uint32_t startingAddress;
	startingAddress = ((row_num*4) + page_num) * EEPROM_PAGE_SIZE;

	// Clear page buffer
	NVMCTRL->CTRLA.reg = NVMCTRL_CTRLA_CMDEX_KEY | NVMCTRL_CTRLA_CMD_PBC;
	while(!(NVMCTRL->INTFLAG.bit.READY));

	// Fill page buffer
	uint32_t len;
	for (len = 0; len < EEPROM_PAGE_SIZE/4; len++) {
		//SerialUSB.print("0x");
		//SerialUSB.print(startingAddress, HEX);
		//SerialUSB.print("\t0x");
		//SerialUSB.println(dataSrc[len], HEX);
		*(uint32_t*)startingAddress = dataSrc[len];
		startingAddress += 4;
	}

	// Command a page write
	NVMCTRL->CTRLA.reg = NVMCTRL_CTRLA_CMDEX_KEY | NVMCTRL_CTRLA_CMD_WP;
	while(!(NVMCTRL->INTFLAG.bit.READY));

	return eepromStatus();
}

//	Erase Row
// Erases a row in memory
void eepromEraseRow(uint32_t row_num) {
	uint32_t rowAddress;
	rowAddress = (row_num*4) * EEPROM_PAGE_SIZE;

	// Write address so we can erase first
	NVMCTRL->ADDR.reg = rowAddress/2; // Apperently 16bit means 32bit/2???
	while(!(NVMCTRL->INTFLAG.bit.READY));

	// Erase data there
	NVMCTRL->CTRLA.reg = NVMCTRL_CTRLA_CMDEX_KEY | NVMCTRL_CTRLA_CMD_ER;
	while(!(NVMCTRL->INTFLAG.bit.READY));
}

//	Print Page
// Reads a page and prints it to the serial monitor
// (Debugging feature!)
void eepromPrintPage(uint32_t row_num, uint32_t page_num) {
	// Read page
	eepromReadPage(row_num, page_num);

	// print page and row #
	SerialUSB.print("\tRow: ");
	SerialUSB.print(row_num);
	SerialUSB.print("\tPage: ");
	SerialUSB.println(page_num);

	// Print page buffer
	uint32_t i;
	for (i = 0; i < EEPROM_PAGE_SIZE/4; i++) {
		SerialUSB.print(i);
		SerialUSB.print(":\t0x");
		SerialUSB.println(pageBuffer[i], HEX);
	}

}

//	Print Row
// Reads a row and prints it to the serial monitor
// (Debugging feature!)
void eepromPrintRow(uint32_t row_num) {
	// Read Row
	eepromReadRow(row_num);

	// print row #
	SerialUSB.print("\tRow: ");
	SerialUSB.println(row_num);


	// Print row buffer in 4 columns
	uint32_t i;
	for (i = 0; i < EEPROM_PAGE_SIZE/4; i++) {
		SerialUSB.print(i);
		SerialUSB.print(":\t0x");
		SerialUSB.print(rowBuffer[i], HEX);
		SerialUSB.print(", ");
		SerialUSB.print("\t0x");
		SerialUSB.print(rowBuffer[i+(EEPROM_PAGE_SIZE/4)], HEX);
		SerialUSB.print(", ");
		SerialUSB.print("\t0x");
		SerialUSB.print(rowBuffer[i+2*(EEPROM_PAGE_SIZE/4)], HEX);
		SerialUSB.print(", ");
		SerialUSB.print("\t0x");
		SerialUSB.print(rowBuffer[i+3*(EEPROM_PAGE_SIZE/4)], HEX);
		SerialUSB.println();
	}
}

// 	Read Status
// Return the status of the NVM controller
uint32_t eepromStatus() {
	return (uint32_t) NVMCTRL->STATUS.reg;
}

//	Get Row Buffer
// Allows other files to get a copy of the row buffer
void eepromCopyRowBuffer(uint32_t *dst, uint32_t len) {
	for (int i = 0; i < len; i++) {
		dst[i] = rowBuffer[i];
	}
}

//	Get Page Buffer
// Allows other files to get a copy of the page buffer
void eepromCopyPageBuffer(uint32_t *dst, uint32_t len) {
	for (int i = 0; i < len; i++) {
		dst[i] = pageBuffer[i];
	}
}
